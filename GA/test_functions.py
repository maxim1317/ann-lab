class Test():
    """docstring for Test"""

    dims = 0
    borders = {
        0: [0, 0],
        1: [0, 0]
    }

    minimum = {
        'coordinates': [
            (0, 0)
        ],
        'value'      : 0
    }

    def _function(self, vec):
        return vec

    def __init__(self):
        # self.prepare()
        self.name = self.__class__.__name__
        return

    def prepare(self):
        self.borders[0][0] += 0.0000001
        self.borders[0][1] -= 0.0000001
        self.borders[1][0] += 0.0000001
        self.borders[1][1] -= 0.0000001

    def score(self, vec):
        return self._function(vec)

    def show(self):
        output = '\n'

        output += self._function.__doc__

        borders_out = 'Borders:'
        x = str(self.borders[0][0]) + ' <= x <= ' + str(self.borders[0][1])
        y = str(self.borders[1][0]) + ' <= y <= ' + str(self.borders[1][1])
        borders_out += '\n\t' + x + '\n\t' + y
        output += '\n' + borders_out

        min_out = 'Global minimum equals ' + str(float(self.minimum['value'])) + \
            ' at point' + 's' * (len(self.minimum['coordinates']) > 1) + ':'
        min_coords = ''
        for c in self.minimum['coordinates']:
            min_coords += '\n\t' + '(' + str(c[0]) + ', ' + str(c[1]) + ')'
        min_out += min_coords

        output += '\n' + min_out
        output += '\n'

        return output


class Himmelblau(Test):
    """docstring for Himmelblau"""

    dims = 3
    borders = {
        0: [-5, 5],
        1: [-5, 5]
    }

    minimum = {
        'coordinates': [
            ( 3.0     ,  2.0     ),
            (-2.805118,  3.131312),
            (-3.779310, -3.283186),
            ( 3.584428, -1.848126)
        ],
        'value'      : 0.0
    }

    def _function(self, vec):
        """Himmelblau function

        f(x,y)=(x^2+y-11)^2+(x+y^2-7)^2
        """
        x = vec[0]
        y = vec[1]
        result = (x ** 2 + y - 11) ** 2 + (x + y ** 2 - 7) ** 2
        return result


class McCormick(Test):
    """docstring for McCormick"""

    dims = 3
    borders = {
        0: [-1.5, 4],
        1: [-3  , 4]
    }

    minimum = {
        'coordinates': [
            (-0.54719, -1.54719)
        ],
        'value'      : -1.9133
    }

    def _function(self, vec):
        """McCormick function

        f(x,y)=sin(x+y)+(x-y)^2-1.5x+2.5y+1
        """
        from math import sin
        x = vec[0]
        y = vec[1]
        result = sin(x + y) + (x - y) ** 2 - 1.5 * x + 2.5 * y + 1
        return result


class Rosenbrock(Test):
    """docstring for Rosenbrock"""

    dims = 3
    borders = {
        0: [-5, 5],
        1: [-5, 5]
    }

    minimum = {
        'coordinates': [
            (1.0, 1.0)
        ],
        'value'      : 0.0
    }

    def _function(self, vec):
        """Rosenbrock function

        f(x,y)=100(y-x^2)^2+(1-x)^2
        """
        x = vec[0]
        y = vec[1]
        result = 100 * (y - x ** 2) ** 2 + (1 - x) ** 2
        return result


class Beale(Test):
    """docstring for Beale"""

    dims = 3
    borders = {
        0: [-4.5, 4.5],
        1: [-4.5, 4.5]
    }

    minimum = {
        'coordinates': [
            (3.0, 0.5)
        ],
        'value'      : 0.0
    }

    def _function(self, vec):
        """Beale function

        f(x,y)=(1.5-x+xy)^2+(2.25-x+xy^2)^2+(2.625-x+xy^3)^2 +(2.625-x+xy^3)^2
        """
        x = vec[0]
        y = vec[1]
        result = (1.5 - x + x * y) ** 2 + (2.25 - x + x * y ** 2) ** 2 + (2.625 - x + x * y ** 3) ** 2 + \
            (2.625 - x + x * y ** 3) ** 2
        return result


class Matyas(Test):
    """docstring for Matyas"""

    dims = 3
    borders = {
        0: [-10, 10],
        1: [-10, 10]
    }

    minimum = {
        'coordinates': [
            (0.0, 0.0)
        ],
        'value'      : 0.0
    }

    def _function(self, vec):
        """Matyas function

        f(x,y)=0.26(x^2+y^2)-0.48xy
        """
        x = vec[0]
        y = vec[1]
        result = 0.26 * (x ** 2 + y ** 2) - 0.48 * x * y
        return result


tests = {
    'Himmelblau': Himmelblau,
    'McCormick' : McCormick,
    'Rosenbrock': Rosenbrock,
    'Beale'     : Beale,
    'Matyas'    : Matyas,
}


if __name__ == '__main__':
    for t in tests.values():
        test = t()
        print(test.show())
