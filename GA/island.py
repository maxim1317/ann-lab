from classic import ClassicGA


class Island(ClassicGA):
    """docstring for Island"""

    def __init__(self, _id, pop_size, function):
        import logging as lg
        import coloredlogs
        # import random

        # random.seed(1)
        self._id = _id

        self.logger = lg.getLogger('Island #' + str(self._id))
        self.logger.setLevel(lg.INFO)

        coloredlogs.install(level='INFO', logger=self.logger)

        self.pop_size = pop_size

        self.function = function

        self.dims = self.function.dims - 1
        self.pop = self.pop_init(self.dims, self.pop_size, self.function.borders)

        self.logger.info('Island #' + str(self._id) + ' created')

        return

