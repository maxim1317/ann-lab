# Генетечиский алгоритм

## Запуск
```
pip3 install -r requirements.txt  # Установка зависимостей
python3 compare.py -h  # Вывод опций запуска 
```

```
usage: compare.py [-h]
                  [--function {Himmelblau,McCormick,Rosenbrock,Beale,Matyas}]

Compare classical GA and island model

optional arguments:
  -h, --help            show this help message and exit
  --function {Himmelblau,McCormick,Rosenbrock,Beale,Matyas}, -f {Himmelblau,McCormick,Rosenbrock,Beale,Matyas}
                        Choose test function or leave empty for random

```
