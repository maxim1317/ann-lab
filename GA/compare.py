import argparse
import logging as lg
from random import choice
from test_functions import tests

from islands import IslandsGA
from classic import ClassicGA


def main():
    parser = argparse.ArgumentParser(description='Compare classical GA and island model')
    parser.add_argument(
        '--function', '-f',
        default=None,
        choices=[
            'Himmelblau',
            'McCormick',
            'Rosenbrock',
            'Beale',
            'Matyas',
        ],
        help='Choose test function or leave empty for random'
    )
    args   = parser.parse_args()

    if args.function is None:
        print('No test function selected, choosing random')
        fn = choice(list(tests.values()))
        function = fn()
        print(function.show())
    else:
        fn = tests[args.function]
        function = fn()
        print(function.show())

    # island_logger = logger.getLogger('ClassicGA')
    islands = IslandsGA(pop_per_isle_size=1000, function=function.name)
    classic = ClassicGA(pop_size=150, function=function.name)

    isl_answer = islands.run(threshold=0.0001)
    cls_answer = classic.run()

    print()
    print('Классической модели понадобилось', classic.gen, 'поколений, чтобы найти минимум')
    print('', cls_answer[1])
    print('', cls_answer[0])
    print()
    print('Островной модели понадобилось', islands.gen, 'поколений, чтобы найти минимум')
    print('', isl_answer[1])
    print('', isl_answer[0])


if __name__ == '__main__':
    main()
