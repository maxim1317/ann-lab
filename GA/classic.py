class ClassicGA(object):
    """docstring for ClassicGA"""

    def __init__(self, pop_size=150, function=None):
        import logging as lg
        import coloredlogs
        from random import choice
        from test_functions import tests
        # import random

        # random.seed(1)

        self.logger = lg.getLogger('ClassicGA')
        self.logger.setLevel(lg.INFO)

        fh = lg.FileHandler('ClassicGA.log')
        fh.setLevel(lg.WARNING)
        ch = lg.StreamHandler()
        ch.setLevel(lg.WARNING)

        formatter = lg.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        # ch.setFormatter(formatter)

        self.logger.addHandler(fh)
        # self.logger.addHandler(ch)

        coloredlogs.install(level='INFO', logger=self.logger)

        self.pop_size = pop_size

        if function is None:
            self.logger.warning('No test function selected, choosing random')
            fn = choice(list(tests.values()))
            self.function = fn()
            self.logger.info(self.function.show())
        else:
            fn = tests[function]
            self.function = fn()
            self.logger.info(self.function.show())

        self.draw_init()

        self.dims = self.function.dims - 1

        self.pop = self.pop_init(self.dims, self.pop_size, self.function.borders)

        self.logger.info('ClassicGA created')

        return

    def run(
        self,
        threshold=0.001,
        max_gen=10000,
        cur_gen=None
    ):

        self.local_minimum = None

        if cur_gen is None:
            self.gen = 0
            cur_gen = 0
        else:
            self.gen = cur_gen

        while self.gen < cur_gen + max_gen:
            self.logger.info('Generation ' + str(self.gen))

            self.scores = self.score(self.function.score, self.pop)

            self.pop, self.scores = self.sort(self.pop, self.scores)

            gm = self.global_minimum_check(self.scores[0], self.function.minimum['value'], threshold)
            if gm:
                winner = self.pop[self.scores.index(gm)]
                self.logger.info(
                    '\n' +
                    'Found global minimum ' + str(gm) +
                    ' at (' + str(winner[0]) + ', ' + str(winner[1]) + ').' +
                    '\n' +
                    self.function.show()
                )
                break

            if self.local_minimum is None or self.scores[0] < self.local_minimum:
                self.local_minimum = self.scores[0]
                self.local_minimum_coords = self.pop[0]
                self.draw_update(self.gen, self.local_minimum, self.local_minimum_coords)

            self.logger.debug(
                '\n' +
                'Best score is ' + str(self.scores[0]) +
                '\n' +
                'Coordinates: ' + str(set(self.pop[0])) +
                '\n' +
                'Target is ' + str(self.function.minimum['value']) +
                '\n' +
                'Coordinates: ' + str(self.function.minimum['coordinates'])
            )

            self.pop = self.select(self.pop)
            self.pop = self.crossover(self.pop)
            self.pop = self.mutate(self.pop, self.function.borders, self.dims)

            self.gen += 1

            # print(self.pop)

            # break

        self.draw_stop()

        return (gm, self.pop[0])

    #####################
    ####### STEPS #######
    #####################

    def pop_init(self, dims, size, borders):
        from random import uniform

        pop = []

        for i in range(size):
            individual = []
            for d in range(dims):
                individual.append(uniform(borders[d][0], borders[d][1]))
            pop.append(individual.copy())

        return pop

    def score(self, score_fn, pop):
        scores = []

        for individual in pop:
            scores.append(score_fn(individual))

        return scores

    def sort(self, pop, scores):
        scores, pop = (list(t) for t in zip(*sorted(zip(scores, pop))))
        return pop, scores

    def global_minimum_check(self, score, true_gm, threshold):
            if abs(score - true_gm) < threshold:
                return score
            else:
                return 0

    def select(self, pop):
        return pop

    def crossover(self, pop, probability=0.9):
        from numpy.random import choice

        skip = choice([0, 1], 1, p=[probability, 1 - probability])
        if skip:
            return pop

        new_pop = []
        for p in range(0, len(pop) - 1, 2):
            parent1 = pop[p]
            parent2 = pop[p + 1]
            child1, child2 = self.make_child(parent1, parent2)
            new_pop.append(child1)
            new_pop.append(child2)

        pop = new_pop.copy()
        return pop

    def make_child(self, parent1, parent2):
        from numpy import array
        from numpy.random import uniform

        p1 = array(parent1)
        p2 = array(parent2)

        alpha1 = uniform(-0.25, 1.25, p1.shape[0])
        alpha2 = uniform(-0.25, 1.25, p2.shape[0])

        c1 = p1 + alpha1 * (p2 - p1)
        c2 = p1 + alpha2 * (p2 - p1)

        return list(c1), list(c2)

    def mutate(self, pop, borders, dims, m=20, probability=0.1):
        from numpy import array, dot
        from numpy.random import choice, random

        skip = choice([0, 1], 1, p=[probability, 1 - probability])
        if skip:
            return pop

        alpha = array([
            0.45 * abs(borders[i][1] - borders[i][0]) for i in range(dims)
        ])

        a = choice([0, 1], dims, p=[1 - 1 / m, 1 / m])
        b = array([2 ** (-i) for i in range(dims)])
        s = dot(a, b)

        for i in range(len(pop)):
            k = 1 if random() < 0.5 else -1
            pop[i] = list(pop[i] + k * s * alpha)

        return pop

    #####################
    ##### STEPS END #####
    #####################

    #####################
    ##### DRAW TOOL #####
    #####################

    def draw_init(self):
        return

    def draw_update(self, gen, f, xy):
        return

    def draw_stop(self):
        return

    #####################
    ### DRAW TOOL END ###
    #####################


def test():
    ga = ClassicGA(
        pop_size=100
    )
    ga.run()


if __name__ == '__main__':
    test()
