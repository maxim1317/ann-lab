class IslandsGA(object):
    """docstring for IslandGA"""

    def __init__(self, islands=5, pop_per_isle_size=1000, function=None):
        import logging as lg
        import coloredlogs
        from random import choice
        from test_functions import tests
        # import random

        # random.seed(1)

        self.logger = lg.getLogger('IslandGA')
        self.logger.setLevel(lg.INFO)

        fh = lg.FileHandler('IslandGA.log')
        fh.setLevel(lg.WARNING)
        ch = lg.StreamHandler()
        ch.setLevel(lg.WARNING)

        formatter = lg.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        # ch.setFormatter(formatter)

        self.logger.addHandler(fh)
        # self.logger.addHandler(ch)

        coloredlogs.install(level='INFO', logger=self.logger)

        self.pop_per_isle_size = pop_per_isle_size

        if function is None:
            self.logger.warning('No test function selected, choosing random')
            fn = choice(list(tests.values()))
            self.function = fn()
            self.logger.info(self.function.show())
        else:
            fn = tests[function]
            self.function = fn()
            self.logger.info(self.function.show())

        self.islands = self.gen_islands(islands, self.pop_per_isle_size, self.function)

        self.logger.info('IslandGA created')

        return

    def run(
        self,
        threshold=0.001,
        gens_per_exchange=5,
        max_gen=10000
    ):
        self.gen = 0

        while self.gen <= max_gen:
            for subgen in range(gens_per_exchange):
                for isl in self.islands:
                    gm = isl.run(
                        threshold=threshold,
                        max_gen=1,
                        cur_gen=self.gen
                    )
                    if gm[0]:
                        break
                if gm[0]:
                    return gm
                self.gen += 1

            self.islands = self.exchange(self.islands)

        return

    #####################
    ####### STEPS #######
    #####################

    def gen_islands(self, num, pop_size, function):
        from island import Island
        islands = [Island(i, pop_size, function) for i in range(num)]

        for i in range(num):
            islands[i].connections = [(i + 1) % num, (i + 3) % num]
        return islands

    def exchange(self, islands):
        for i in range(len(islands)):
            for c in islands[i].connections:
                islands[c].pop[0] = self.make_child(islands[i].pop[0], islands[c].pop[0])
                islands[c].pop[1] = self.make_child(islands[i].pop[0], islands[c].pop[0])
        return islands

    def make_child(self, parent1, parent2):
        from numpy import array
        from numpy.random import uniform

        p1 = array(parent1)
        p2 = array(parent2)

        alpha1 = uniform(-0.25, 1.25, p1.shape[0])

        c1 = p1 + alpha1 * (p2 - p1)\

        return list(c1)

    #####################
    ##### STEPS END #####
    #####################

    #####################
    ##### DRAW TOOL #####
    #####################

    def draw_init(self):
        return

    def draw_update(self, gen, f, xy):
        return

    def draw_stop(self):
        return

    #####################
    ### DRAW TOOL END ###
    #####################


def test():
    ga = IslandsGA(
        islands=5,
        pop_per_isle_size=1000
    )
    ga.run()


if __name__ == '__main__':
    test()
