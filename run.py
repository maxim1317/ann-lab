"""Run KNN."""
from knn.knn import run as knn_run
# from id3.id3 import run as id3_run


def run():
    """Run."""
    knn_run()
    # id3_run()


if __name__ == '__main__':
    run()
