from dataset import *


def test_1():

    ds = Dataset(json='Generated/2d_50cl_25000p_7190s.json')
    ds.plot()


def test_2():
    # pp = pprint.PrettyPrinter(indent=4)

    ds = Dataset(dims=2, classes=2, per_class=500, border=10, noise=0, intersections=0)

    data = ds.generate()

    # ds.plot(plot_l=True)

    ds.to_json()
    ds.to_csv('out_test')
    ds.plot()

    return data
    # pp.pprint(data)


if __name__ == '__main__':
    test_2()
