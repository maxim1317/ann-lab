import numpy as np
import random as r

from utils import *


def gen_centroids(
    dims,
    classes,
    border
):
    """Generate cluster centers

    Args:
        dims (int): Dimensions
        classes (int): Number of classes
        border (float): Hypercube borders

    Returns:
        Dict of Dicts: of Centroids

        Example:
        {
            0: {
                'center': ndarray <center_coordinates>
                'radius': float
            },
            ...
        }
    """
    from tqdm import tqdm
    from math import sqrt

    c_bar = tqdm(total=classes)

    centroids = {}

    min_rad = 2 * border / ((sqrt(classes)) * 7)  # Minimal claster radius
    max_rad = 2 * border / ((sqrt(classes)) * 4)   # Max claster radius

    # min_rad = 2 * border / (10000)  # Minimal claster radius
    # max_rad = 2 * border / (1000)   # Max claster radius

    # Generating central point at [0.0 0.0 ...]
    centroids[0] = {
        'center': np.array([0.0 for i in range(dims)]),
        'radius': r.uniform(min_rad, max_rad)
    }

    c_bar.update()

    # Generating centers for other classes
    for cl in range(1, classes):
        potential_point = [r.uniform(-border, border) for i in range(dims)]  # Populating with random numbers

        check = False

        c_bar.set_description(desc='Class ' + str(cl))

        #  Repopulating if new center is too close to others
        while not check:
            min_dist = 3 * border  # Big enough number

            check = True

            # Going througn generated centroids
            for cl_n, c in centroids.items():
                dist = find_distance(potential_point, c['center'])  # Finding distance between two centers
                # print(cl, ' ', dist, ' ', min_rad + c['radius'], ' ', min_dist)
                if dist < (min_rad + c['radius']):  # if distance between centers is less than minimal radius + radius of centroid, then
                    potential_point = [r.uniform(-border, border) for i in range(dims)]  # repopulating center with random classes
                    check = False                                                        # and
                    continue                                                             # exiting cycle
                else:
                    if dist < (min_dist + c['radius']):
                        min_dist = dist - c['radius']  # counting minimal distance to other classes

            if min_dist == 3 * border:
                potential_point = [r.uniform(-border, border) for i in range(dims)]  # repopulating center with random classes
                check = False

        if min_dist > max_rad:                          # if center is too far from any class
            radius = r.uniform(min_rad, max_rad)         # just give it random radius
        else:                                            # if not
            radius = min_dist  # make it as large as possible

        # Saving
        centroids[cl] = {
            'center': potential_point,
            'radius': radius
        }

        c_bar.update()

    final_centroids = {}

    rads = []
    inds = []

    for cl, c in centroids.items():
        rads.append(c['radius'])
        inds.append(cl)

    rads, inds = zip(*sorted(zip(rads, inds)))

    # rads = list(rads)

    for i in range(classes):
        final_centroids[i] = centroids[inds[i]]

    c_bar.close()

    return final_centroids


def gen_noise(
    dims,
    classes,
    centroids,
    border,
    noise=1,
    intersections=1
):
    if not noise:
        return centroids

    locked = []

    for intrsct in range(intersections):
        shortest = 3 * border

        for cli in range(classes - 1):
            for clj in range(cli + 1, classes):
                if clj in locked:
                    continue
                dist = find_distance(centroids[cli]['center'], centroids[clj]['center']) - \
                    centroids[cli]['radius'] - centroids[clj]['radius']

                if dist < shortest:
                    shortest = dist
                    c_dist = dist + centroids[cli]['radius'] + centroids[clj]['radius']
                    min_cl = [cli, clj]

        cli, clj = min_cl

        vec = find_vec(centroids[cli]['center'], centroids[clj]['center'])

        offset = vec * shortest / c_dist
        print(centroids[cli]['center'], centroids[clj]['center'], offset, shortest)

        centroids[clj]['center'] = (np.array(centroids[clj]['center']) - offset).tolist()

        vec = find_vec(centroids[cli]['center'], centroids[clj]['center'])
        print(centroids[cli]['center'], centroids[clj]['center'])
        offset = 2 * vec * noise * min(centroids[cli]['radius'], centroids[clj]['radius']) / (centroids[cli]['radius'] + centroids[clj]['radius'])

        centroids[clj]['center'] = (np.array(centroids[clj]['center']) - offset).tolist()
        print(
            ' cli', centroids[cli]['center'], centroids[cli]['radius'], '\n',
            'clj', centroids[clj]['center'], centroids[clj]['radius'], '\n',
            'offset', offset, '\n',
            'dist', find_distance(centroids[cli]['center'], centroids[clj]['center'])
        )

        locked.append(clj)
        if cli not in locked:
            locked.append(cli)

        if len(locked) >= intersections:
            break

    return centroids


def gen_points(
        dims,
        classes,
        per_class,
        centroids
):
    from tqdm import tqdm

    p_bar = tqdm(total=classes)

    points = {}

    for cl in range(classes):
        points[cl] = gen_sphere_points(
            dims=dims,
            per_class=per_class,
            centroid=centroids[cl]
        )
        p_bar.update()

    p_bar.close()

    return points


def gen_sphere_points(
    dims,
    per_class,
    centroid
):
    import math as m

    points = []

    # radius = r.triangular(0, centroid['radius'], 0.1)

    for p in range(per_class):
        radius = r.random() * centroid['radius']

        coords = []
        phis   = []

        for d in range(dims):
            phi = r.uniform(0, 2 * m.pi)
            phis.append(phi)

        x = radius

        for d in range(dims):
            x *= m.sin(phis[d])

        coords.append(x)

        for dim in range(1, dims):
            x = radius * m.cos(phis[dim - 1])

            for d in range(dim, dims):
                x *= m.sin(phis[d])

            coords.append(x)

        coords = (np.array(coords) + np.array(centroid['center'])).tolist()

        points.append(coords.copy())

    return points


def gen_dict(
    dims,
    classes,
    per_class,
    noise,
    intersections,
    border,
    seed,
    centroids,
    points
):
    generated = {
        'dims'          : dims,
        'classes'       : classes,
        'per_class'     : per_class,
        'noise'         : noise,
        'intersections' : intersections,
        'border'        : border,
        'seed'          : seed,
        'centroids'     : centroids,
        'points'        : points
    }

    return generated
