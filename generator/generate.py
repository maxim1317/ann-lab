import argparse
from dataset import Dataset


def dim_type(x):
    x = int(x)
    if x < 2:
        raise argparse.ArgumentTypeError("Minimum number of features is 2")
    if x > 20:
        raise argparse.ArgumentTypeError("Maximum number of features is 20")
    return x


def cl_type(x):
    x = int(x)
    if x < 2:
        raise argparse.ArgumentTypeError("Minimum number of classes is 2")
    if x > 100:
        raise argparse.ArgumentTypeError("Maximum number of classes is 100")
    return x


def p_type(x):
    x = int(x)
    if x < 1:
        raise argparse.ArgumentTypeError("Minimum number of points per class is 1")
    if x > 1000:
        raise argparse.ArgumentTypeError("Maximum number of points per class is 1000")
    return x


def b_type(x):
    x = float(x)
    if x < 1:
        raise argparse.ArgumentTypeError("Minimum border is 1")
    return x


def n_type(x):
    x = float(x)
    if x < 0:
        raise argparse.ArgumentTypeError("Minimum amount of noise is 0")
    if x > 1:
        raise argparse.ArgumentTypeError("Maximum amount of noise is 1")
    return x


def i_type(x):
    x = int(x)
    if x < 0:
        raise argparse.ArgumentTypeError("Minimum number of intersections is 0")
    return x


def ensure_dir(directory):
    import os
    if not os.path.exists(directory):
        os.makedirs(directory)
    return


def main():
    import os

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-f",
        "--features",
        help="Set number of features [2, 20], defaults to 2",
        type=dim_type,
        default=2
    )
    parser.add_argument(
        "-c",
        "--clusters",
        help="Set number of clusters [2, 100], defaults to 2",
        type=cl_type,
        default=2
    )
    parser.add_argument(
        "-d",
        "--dots",
        help="Set number of dots per cluster [1, 1000], defaults to 100",
        type=p_type,
        default=100
    )
    parser.add_argument(
        "-n",
        "--noise",
        help="Set amount of noise [0, 1], defaults to 0",
        type=n_type,
        default=0
    )
    parser.add_argument(
        "-i",
        "--intersections",
        help="Set number of intersections, defaults to 0",
        type=i_type,
        default=0
    )
    parser.add_argument(
        "-b",
        "--border",
        help="Set maximum coordinate, defaults to 10",
        type=b_type,
        default=10
    )
    parser.add_argument(
        "-j",
        "--json",
        help="Import dataset from json",
        type=str,
        default=None
    )
    parser.add_argument(
        "-o",
        "--out",
        help="Output location for csv files, defaults to None",
        type=str,
        default=None
    )
    parser.add_argument(
        "-p",
        "--plot",
        help="Plot result",
        action="store_true",
        default=False
    )

    args = parser.parse_args()
    ds = Dataset(
        dims=args.features,
        classes=args.clusters,
        per_class=args.dots,
        noise=args.noise,
        intersections=args.intersections,
        border=args.border,
        json=args.json
    )

    ds.generate()

    ensure_dir('Generated')

    ds.to_json()
    if args.out:
        ensure_dir(args.out)
        ds.to_csv(args.out)
    if args.plot:
        ds.plot()

    return


if __name__ == '__main__':
    main()
