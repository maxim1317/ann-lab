import random as r
from termcolor import colored

from utils import *


class Dataset():
    """Dataset class

    Class for generating, storing, spliting datasets

    Args:
        dims        (int, optional):
            Number of dimensions, defaults to 2 dimensions
        classes     (int, optional):
            Number of classes, defaults to 2 classes
        per_class   (int, optional):
            Number of points per class, defaults to 100 points
        noise       (int, optional):
            Amount of mixing between classes, defaults to 0 (no mixing)
        border      (int, optional):
            Specify outer borders of dataset, defaults to 10 (range [-10, 10])
    """

    def __init__(
        self,
        dims=2,
        classes=2,
        per_class=250,
        noise=0,
        intersections=0,
        border=10,
        json=None
    ):

        if json is None:
            self.dims           = dims
            self.classes        = classes
            self.per_class      = per_class
            self.noise          = noise
            self.intersections  = intersections
            self.border         = border

            self.data           = None

        else:
            self.from_json(json)

    def generate(
        self,
        seed=r.randint(0, 20000)
    ):
        """Dataset.plane_generator()

        Generates dataset

        Args:
            seed (int, optional): You can specify random seed

        Returns:
            dict: Dictionary with dataset and other info
        """
        import sphere_generator as sg

        self.seed = seed

        if (
            self.dims < 2 or self.dims > 30
        ):
            print(colored('    ! ! ! ЕГГОГ ! ! !', 'red'))
            return

        if (
            self.classes < 2 or self.classes > 100
        ):
            print(colored('    ! ! ! ЕГГОГ ! ! !', 'red'))
            return

        self.data = sg.generate(
            dims=self.dims,
            classes=self.classes,
            per_class=self.per_class,
            noise=self.noise,
            intersections=self.intersections,
            border=self.border,
            seed=self.seed
        )

        return self.data

    def plot(self, show=True):
        """Dataset.plot

        Args:
            dots    (Dict)          : Dictionary of points with specified class numbers
            border  (int , optional): Borders of the plot, defaults to [-10, 10] range
            show    (bool, optional): Draw plot inside the function, defaults to True

        Returns:
            Nothing
        """
        if not self.data:
            print('    No data to plot!')
            return

        if self.dims > 3:
            print('Can only draw up to 3 dimensions')

        elif self.dims == 3:
            plot_3d(dots=self.data['points'], border=self.border, show=show)

        else:
            plot_2d(dots=self.data['points'], border=self.border, show=show)

        return

    def from_json(self, path):

        self.data = json_to_dict(path)

        self.dims           = self.data['dims']
        self.classes        = self.data['classes']
        self.per_class      = self.data['per_class']
        self.noise          = self.data['noise']
        self.intersections  = self.data['intersections']
        self.border         = self.data['border']

        self.points = {}

        for cl in range(self.classes):
            self.points[cl] = self.data['points'][str(cl)]

        self.data['points'] = self.points

        return self.data

    def to_json(self, path=''):
        from os.path import join, abspath
        if not self.data:
            print('    No data to write !')
            return

        if path == '':
            fname = \
                str(self.dims) + 'd' + \
                '_' + \
                str(self.classes) + 'cl' + \
                '_' + \
                str(self.classes * self.per_class) + 'p' + \
                '_' + \
                str(self.seed) + 's' + \
                '_' + \
                str(self.noise) + 'n' + \
                '.json'

            path = abspath(join('Generated', fname))

        dict_to_json(self.data, path)
        return

    def to_csv(self, directory, test=0.3, val=0):
        import csv
        from os.path import join

        points = self.data['points']

        test_per_class  = int(self.per_class * test)
        val_per_class   = int(self.per_class * val)
        train_per_class = int(self.per_class * (1 - test - val))

        test_data  = []
        val_data   = []
        train_data = []

        for cl in range(self.classes):
            dots = points[cl].copy()

            r.shuffle(dots)

            for t in range(test_per_class):
                line = dots.pop() + [cl]
                test_data.append(line.copy())
            for v in range(val_per_class):
                line = dots.pop() + [cl]
                val_data.append(line.copy())
            for t in range(train_per_class):
                line = dots.pop() + [cl]
                train_data.append(line.copy())

        r.shuffle(test_data)
        r.shuffle(val_data)
        r.shuffle(train_data)

        labels = []

        for dim in range(self.dims):
            labels.append('X_' + str(dim))

        labels.append('cluster')

        with open(join(directory, 'test.csv'), 'w') as test_file:
            w = csv.writer(test_file)

            w.writerow(labels)

            for line in range(len(test_data)):
                w.writerow(test_data[line])

        if val :
            with open(join(directory, 'val.csv'), 'w') as val_file:
                w = csv.writer(val_file)

                w.writerow(labels)

                for line in range(len(val_data)):
                    w.writerow(val_data[line])

        with open(join(directory, 'train.csv'), 'w') as train_file:
            w = csv.writer(train_file)

            w.writerow(labels)

            for line in range(len(train_data)):
                w.writerow(train_data[line])

        return
