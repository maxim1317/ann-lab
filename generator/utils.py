import json
import numpy as np
import matplotlib.pyplot as plt


def plot_2d(dots, border=10, show=True):
    import matplotlib.pyplot as plt

    plt.xkcd()

    dotx  = []
    doty  = []
    dotsp = []
    dotc  = []

    for cl in dots.keys():
        for dot in dots[cl]:
            dotx.append(dot[0])
            doty.append(dot[1])
            dotsp.append(1)
            dotc.append(cl + 5)

    plt.scatter(dotx, doty, dotsp, c=tuple(dotc), cmap=discrete_cmap(len(dotc) + 10, 'gist_rainbow'))

    if show:
        plt.show()
    # plt.show()

    return


def plot_3d(dots, border=10, show=True):
    from mpl_toolkits.mplot3d import Axes3D

    plt.xkcd()

    plt3d = plt.figure().gca(projection='3d')

    plt3d.set_xlim((-border, border))
    plt3d.set_ylim((-border, border))
    plt3d.set_zlim((-border, border))

    dotx = []
    doty = []
    dotz = []
    dotc = []

    for cl in dots.keys():
        for dot in dots[cl]:
            dotx.append(dot[0])
            doty.append(dot[1])
            dotz.append(dot[2])
            dotc.append(cl)

    plt3d.scatter(dotx, doty, dotz, c=tuple(dotc), cmap=discrete_cmap(len(dotc) + 2, 'gist_rainbow'))

    if show:
        plt.show()

    return


def find_centroid(points):
    centroid = np.array(points[0])

    for p in range(1, len(points)):
        # print(centroid)
        centroid += np.array(points[p])

    return list(centroid / (len(points)))


def find_distance(p_1, p_2):
    distance = np.linalg.norm(np.array(p_2) - np.array(p_1))
    return distance


def find_vec(p_1, p_2):
    vec = np.array(p_2) - np.array(p_1)
    return vec


def discrete_cmap(N, base_cmap=None):
    """Create an N-bin discrete colormap from the specified input map"""

    # Note that if base_cmap is a string or None, you can simply do
    #    return plt.cm.get_cmap(base_cmap, N)
    # The following works for string, None, or a colormap instance:

    base = plt.cm.get_cmap(base_cmap)
    color_list = base(np.linspace(0, 1, N))
    cmap_name = base.name + str(N)
    return base.from_list(cmap_name, color_list, N)


def json_to_dict(json_path):
    '''
        Reads JSON file and returns a dict
    '''
    from json import loads

    with open(json_path) as raw:
        jdict = loads(raw.read())
    return jdict


def dict_to_json(jdict, json_path):
    '''
        Writes dict as JSON to file
    '''
    from json import dump

    with open(json_path, 'w') as raw:
        dump(jdict, raw, indent=4, sort_keys=False, ensure_ascii=False, cls=NumpyEncoder)
    return


def print_steps(desc, status):
    import os
    from termcolor import colored

    try:
        rows, columns = os.popen('stty size', 'r').read().split()
    except ValueError:
        columns = 80

    if status == 'wait':
        print("\n", desc, end=((int(columns) - 3 - len(desc)) * " " + "[" + colored("  WAIT  ", 'yellow') + "]\n"))
    elif status == 'ok':
        print("", desc, end=((int(columns) - 3 - len(desc)) * " " + "[" + colored("   OK   ", 'green') + "]\n"))
    elif status == 'fail':
        print("", desc, end=((int(columns) - 3 - len(desc)) * " " + "[" + colored(" FAILED ", 'red') + "]\n"))
    return


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)
