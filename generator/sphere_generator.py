import random as r
from termcolor import colored

from sg_internals import *
from utils import *


def generate(
    dims,
    classes,
    per_class,
    noise,
    intersections,
    border,
    seed=r.randint(0, 20000)
):

    r.seed(seed)

    steps = get_steps()

    print_steps(steps['centroids'], 'wait')
    centroids = gen_centroids(
        dims=dims,
        classes=classes,
        border=border
    )
    print_steps(steps['centroids'], 'ok')

    print_steps(steps['noise'], 'wait')
    centroids = gen_noise(
        dims=dims,
        classes=classes,
        border=border,
        centroids=centroids,
        noise=noise,
        intersections=intersections
    )
    print_steps(steps['noise'], 'ok')

    print_steps(steps['points'], 'wait')
    points = gen_points(
        dims=dims,
        classes=classes,
        per_class=per_class,
        centroids=centroids
    )
    print_steps(steps['points'], 'ok')

    print_steps(steps['data'], 'wait')
    data = gen_dict(
        dims=dims,
        classes=classes,
        per_class=per_class,
        noise=noise,
        intersections=intersections,
        border=border,
        seed=seed,
        centroids=centroids,
        points=points
    )
    print_steps(steps['data'], 'ok')

    return data


def get_steps():

    steps = {
        'centroids':
            'Generating cluster ' +
            colored(
                'centroids',
                'yellow'
            ) +
            '...',
        'points':
            'Generating ' +
            colored(
                'points',
                'yellow'
            ) +
            '...',
        'noise':
            'Calculating some ' +
            colored(
                'noise',
                'yellow'
            ) +
            '...',
        'data':
            'Generating ' +
            colored(
                'output dictionary',
                'yellow'
            ) +
            '...',
    }

    return steps
