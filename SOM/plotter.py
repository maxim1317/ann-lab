def hex_mapper(q, size):  # Тебе q не нужна
    import logging
    import numpy as np
    import matplotlib.pyplot as plt

    logging.getLogger("matplotlib").setLevel(logging.WARNING)  # Тут забей, это убрать логи

    fig = plt.gcf()     # Тут тож забей
    fig.show()          # это я юзаю для
    fig.canvas.draw()   # отрисовки в реальном времени

    plt.xkcd()          # гыгык

    xx, yy = get_grid(size)  # Тут спускаемся вниз, к самой функции

    x = xx.ravel()  #
    y = yy.ravel()  # Выпрямляем всё в одномерные массивы
    while True:         # Это опять про "в реальном времени"

        got = q.get()   # Тут я из трубы получаю инфу

        if got is None:     # блаблабла
            break           # блаблабла
        plt.clf()

        grid, epoch, loc = got  # Распаковываем посылку

        plt.title('Epoch ' + str(epoch))  # Пишем заголовок

        ########################
        # Вот отсюда интересно #
        ########################

        # grid - это наш массив, где в каждой клетке число [0, 255].
        cc = np.array(grid)  # привёл к нумпи, чтоб было легче дальше

        c = cc.ravel()  #

        # plt.hexbin(
        #     x, y,               # Так, тут просят одномерные массивы, их мы заготовили
        #     C=c,                # Тут мы перегружаем встроенный подсчёт hexbin своими значениями
        #     gridsize=size - 5,  # Указываем размер сетки. 4 - волшебная константа, которая не даёт сетке порваться
        #     cmap='inferno'      # Цвета
        # )

        plt.pcolormesh(xx, yy, cc, cmap='inferno')

        plt.gca().set_aspect('equal', adjustable='box')

        plt.draw()          # Это тоже тебе не нужно
        fig.canvas.draw()   # снова отрисовка покадравая
        if loc is not None:
            plt.savefig(loc)
        plt.pause(0.01)

    plt.show()  # Вот и всё

    return


def cluster_mapper(q, data):
    import logging
    import networkx as nx
    import matplotlib.pyplot as plt

    logging.getLogger("matplotlib").setLevel(logging.WARNING)

    fig = plt.gcf()
    fig.show()
    fig.canvas.draw()

    # plt.xkcd()

    testx  = []
    testy  = []
    testsp = []
    testc  = []


    for test in data:
        testx.append(test[0])
        testy.append(test[1])
        testsp.append(36)
        testc.append(test[2])
    # plt.scatter(dotx, doty, dotsp, c=tuple(dotc), marker='v', cmap=discrete_cmap(len(dotc) + 10, 'gist_rainbow'))
    while True:
        got = q.get()

        if got is None:
            break
        plt.clf()

        G, epoch = got

        plt.title('Epoch ' + str(epoch))

        pos = nx.get_node_attributes(G, 'pos')

        # print(max(xx))
        # print(max(yy))
        # print(max(testx))
        # print(max(testy))

        plt.scatter(testx, testy, testsp, c=tuple(testc), marker='o', cmap='gist_rainbow', edgecolors='white')

        nx.draw_networkx_edges(G, pos, width=1.0, alpha=0.5)
        nx.draw_networkx_nodes(
            G, pos,
            node_color='black',
            node_size=2,
            alpha=0.8
        )
        # plt.scatter(testx, testy, testsp, c=tuple(testc), marker='o', cmap=discrete_cmap(len(testc) + 10, 'gist_rainbow'))
        # plt.scatter(dotx, doty, dotsp, c=tuple(dotc), marker='v', cmap=discrete_cmap(len(dotc) + 10, 'gist_rainbow'), edgecolors='black')
        plt.draw()
        fig.canvas.draw()
        plt.pause(0.01)

    plt.show()

    return


def fire_mapper(q):
    import logging
    import matplotlib.pyplot as plt

    logging.getLogger("matplotlib").setLevel(logging.WARNING)

    fig = plt.gcf()
    fig.show()
    fig.canvas.draw()

    # plt.xkcd()

    # clc  = []

    while True:
        got = q.get()

        if got is None:
            break
        plt.clf()

        datx  = []
        daty  = []
        datsp = []
        datc  = []

        clx  = []
        cly  = []
        clsp = []

        data, centroids, iteration = got

        # print(data[0][2])

        for dat in data:
            datx.append(dat[0])
            daty.append(dat[1])
            datsp.append(36)
            datc.append(dat[2])

        for cl in centroids:
            clx.append(cl[0])
            cly.append(cl[1])
            clsp.append(36)
            # clc.append(cl[2])

        plt.title('Iteration ' + str(iteration))

        plt.scatter(datx, daty, datsp, c=tuple(datc), marker='o', cmap='gist_rainbow', edgecolors='white')
        plt.scatter(clx, cly, clsp, c='black', marker='x')

        # plt.scatter(testx, testy, testsp, c=tuple(testc), marker='o', cmap=discrete_cmap(len(testc) + 10, 'gist_rainbow'))
        # plt.scatter(dotx, doty, dotsp, c=tuple(dotc), marker='v', cmap=discrete_cmap(len(dotc) + 10, 'gist_rainbow'), edgecolors='black')
        plt.draw()
        fig.canvas.draw()
        plt.pause(0.01)

    plt.show()

    return


def get_grid(size):
    """Получение сетки

    Очень хитрая штука для моего понимания
    Возвращает два двумерных массива, которые при выпрямлении дают
    !!! индексы прохода по массиву !!!

    Пример:
    xx, yy = get_grid(2)

    print('xx =', xx.ravel())
    print('yy =', yy.ravel())

    Вывод:
    xx = [0 1 0 1]
    yy = [0 0 1 1]

    Args:
        size (int): Размер массива
    """
    import numpy as np

    x_min = y_min = 0
    x_max = y_max = size

    return np.meshgrid(np.arange(x_min, x_max, 1), np.arange(y_min, y_max, 1))
