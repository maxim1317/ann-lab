import numpy as np


class Fireplace(object):
    """docstring for Fireplace"""

    def __init__(self, from_file=None):
        import logging as lg
        import coloredlogs
        from os.path import dirname
        # import random

        # random.seed(1)

        self.logger = lg.getLogger('Fireplace')
        self.logger.setLevel(lg.DEBUG)

        fh = lg.FileHandler('Fireplace.log')
        fh.setLevel(lg.WARNING)
        ch = lg.StreamHandler()
        ch.setLevel(lg.WARNING)

        formatter = lg.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        # ch.setFormatter(formatter)

        self.logger.addHandler(fh)
        # self.logger.addHandler(ch)

        coloredlogs.install(level='DEBUG', logger=self.logger)

        if from_file is None:
            self.logger.info('Fireplace created')
        else:
            self.load(from_file)
            self.save_dir = dirname(from_file)
            self.logger.info('Fireplace loaded')

    def save(self):
        pass

    def load(self):
        pass

    def learn(self, data_path, metric='euclidean', max_iter=1000):
        import multiprocessing
        from plotter import fire_mapper

        self.data = self.read_csv(data_path)
        self.data_path = data_path

        self.centroids = []
        for c in range(self.classes):
            self.centroids.append(self.take_one(self.data)[:self.dims])

        it = 0

        self.q_fire = multiprocessing.Queue()
        p = multiprocessing.Process(target=fire_mapper, args=(self.q_fire,))
        p.start()

        self.q_fire.put((self.data, self.centroids, it))

        while it <= max_iter:
            self.logger.info('Iteration ' + str(it))

            clusters, self.data, changed = self.markup(self.data, self.centroids, metric)

            # for i, c in clusters.items():
            #   print('C', i, len(c))

            if not changed:
                self.logger.info('Quitting')
                self.q_fire.put((self.data, self.centroids, it))
                break

            self.centroids = self.update_centroids(self.centroids, clusters)

            self.q_fire.put((self.data, self.centroids, it))
            it += 1
        self.q_fire.put(None)

        self.score()
        return

    def update_centroids(self, centroids, clusters):
        for i in range(len(centroids)):
            # print(i, clusters[i])
            cluster = np.array(clusters[i])
            centroids[i] = list(np.mean(cluster, axis=0))
            centroids[i] = np.array(centroids[i][:self.dims])
        return centroids

    def markup(self, data, centroids, metric='euclidean', return_labels=False):

        distances = {
            "euclidean": self.euclidean,
            "manhatten": self.manhatten,
            "chebyshev": self.chebyshev
        }

        changed = False

        clusters = {}
        for d in range(len(centroids)):
            clusters[d] = []

        if return_labels:
            labels = []

        for dot in range(len(data)):
            min_dist = None
            # print(centroids)
            for c_i in range(len(centroids)):
                # print(centroids[c_i], data[dot])
                dist = distances[metric](centroids[c_i], data[dot][:self.dims])
                if min_dist is None or dist <= min_dist:
                    min_dist = dist
                    min_c    = c_i
            if data[dot].shape[0] == self.dims:
                tmp = list(data[dot])
                tmp.append(min_c)
                changed = True
                data[dot] = np.array(tmp)
            else:
                if data[dot][self.dims] != min_c:
                    data[dot][self.dims] = min_c
                    changed = True

            if return_labels:
                labels.append(min_c)

            tmp = list(clusters[min_c])
            tmp.append(data[dot])
            clusters[min_c] = tmp

        if return_labels:
            return labels
        return clusters, data, changed

    def take_one(self, data):
        import random

        rand_index = random.randint(0, len(data) - 1)
        vec = data[rand_index]

        return vec

    def euclidean(self, vec1, vec2):
        from scipy.spatial.distance import euclidean
        return euclidean(vec1, vec2)

    def manhatten(self, vec1, vec2):
        from scipy.spatial.distance import cityblock
        return cityblock(vec1, vec2)

    def chebyshev(self, vec1, vec2):
        from scipy.spatial.distance import chebyshev
        return chebyshev(vec1, vec2)

    def read_csv(self, filename, labels=True, return_labels=False):
        import csv

        self.logger.debug('Reading dataset from ' + filename)

        data = []

        with open(filename, 'r') as f:
            self.classes = 0
            reader = csv.reader(f)
            if return_labels:
                labels_arr = []
            for row in reader:
                if 'X_0' in row:
                    self.dims = len(row) - 1
                    continue
                try:
                    for c in range(len(row) - 1):
                        row[c] = float(row[c])
                    row[-1] = int(row[-1])
                    if return_labels:
                        labels_arr.append(row[-1])
                    if row[-1] + 1 > self.classes:
                        self.classes = row[-1] + 1
                    row[-1] = 0
                    if not labels:
                        row.pop()
                except ValueError as e:
                    raise
                    continue

                data.append(np.array(row))

        self.logger.debug('Dataset loaded')

        self.logger.debug('Possible clusters: ' + str(self.classes))

        if return_labels:
            return data, labels_arr
        return data

    def score(self):
        from sklearn import metrics

        data, targets = self.read_csv(self.data_path, return_labels=True)
        predicts      = self.markup(data, self.centroids, return_labels=True)

        no_label_data = self.read_csv(self.data_path, labels=False)

        self.logger.info('   Adjusted Rand score: ' + str(metrics.adjusted_rand_score(targets, predicts)))
        self.logger.info('      Silhouette score: ' + str(metrics.silhouette_score(no_label_data, predicts)))
        self.logger.info('Calinski-Harabaz index: ' + str(metrics.calinski_harabaz_score(no_label_data, predicts)))
        self.logger.info('  Davies-Bouldin index: ' + str(metrics.davies_bouldin_score(no_label_data, predicts)))
        return


def test():
    f = Fireplace()
    f.learn(data_path='../MLP/datasets/3_classes/train.csv')


if __name__ == '__main__':
    test()
