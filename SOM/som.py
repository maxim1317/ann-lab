import numpy as np


class SOM(object):
    """docstring for SOM"""

    def __init__(self, size=0, from_file=None):
        import logging as lg
        import coloredlogs
        from os.path import dirname
        # import random

        # random.seed(1)

        self.logger = lg.getLogger('SOM')
        self.logger.setLevel(lg.DEBUG)

        fh = lg.FileHandler('SOM.log')
        fh.setLevel(lg.WARNING)
        ch = lg.StreamHandler()
        ch.setLevel(lg.WARNING)

        formatter = lg.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        # ch.setFormatter(formatter)

        self.logger.addHandler(fh)
        # self.logger.addHandler(ch)

        coloredlogs.install(level='DEBUG', logger=self.logger)

        if from_file is None:
            self.size  = size

            self.grid = self.create_grid(self.size)

            self.logger.info('SOM created')
        else:
            self.load(from_file)
            self.save_dir = dirname(from_file)
            self.logger.info('SOM loaded')

        return

    def create_grid(self, size):
        from node import Node

        grid = []
        for r in range(size):
            column = []
            for c in range(size):
                column.append(Node((c, r)))
            grid.append(column.copy())

        return grid

    def train(
        self,
        data_path=None,
        sigma=None,
        learning_rate=0.1,
        max_iter=100000,
        continue_train=False,
        plot_hex=True,
        plot_clusters=True
    ):
        """Trains the SOM picking samples at random from data.
        Parameters
        ----------
        data : np.array or list
            Data matrix.
        num_iterations : int
            Maximum number of iterations (one iteration per sample).
        verbose : bool (default=False)
            If True the status of the training
            will be printed at each iteration.
        """
        import time
        import multiprocessing
        from os.path import join

        from plotter import hex_mapper, cluster_mapper
        from utils import ensure_dir

        self.logger.info('Training started')

        if not continue_train:
            self.save_dir = join('checkpoints', 'som_' + str(int(time.time())))
            ensure_dir(self.save_dir)

            self.data_path = data_path

            if sigma is None:
                self.sigma = self.size / 2
            else:
                self.sigma = sigma

            self.max_iter     = max_iter

            self.base_sigma    = self.sigma

            self.base_learning = learning_rate
            self.learning_rate = learning_rate

            self.minsigma = self.base_sigma * np.exp(-self.max_iter / (self.max_iter / np.log(self.base_sigma)))
            self.minsigma = 1
            self.minlr    = 0.01

            self.tau1 = self.max_iter / np.log(self.base_sigma)
            self.tau2 = self.max_iter

            # self.tau1 = 1000 / np.log(self.base_sigma)
            # self.tau2 = 1000

            data = self.read_csv(data_path)

            self.initialize(data)

            self.iteration = 0
        else:
            data = self.read_csv(self.data_path)

        ensure_dir(join(self.save_dir, 'pics'))
        # print(dir(SOM))

        if self.dims != 2:
            plot_clusters = False

        self.q_hex = multiprocessing.Queue()
        p = multiprocessing.Process(target=hex_mapper, args=(self.q_hex, 2 * self.size - 1))
        p.start()

        if plot_clusters:
            self.q_clusters = multiprocessing.Queue()
            c = multiprocessing.Process(target=cluster_mapper, args=(self.q_clusters, self.read_csv(self.data_path, labels=True)))
            c.start()

        self.iteration = 0

        umatrix = self.build_umatrix()
        self.q_hex.put((umatrix, self.iteration, None))

        if plot_clusters:
            graph  = self.grid_to_graph()
            self.q_clusters.put((graph, self.iteration))

        self.logger.info('Epoch ' + str(self.iteration))
        while self.iteration <= self.max_iter:
            self.logger.debug('Iteration ' + str(self.iteration))
            vec = self.take_one(data)

            winner = self.winner(vec)

            self.update_weights(winner, vec, self.sigma, self.learning_rate)

            self.sigma = max(self.minsigma, self.base_sigma * np.exp(-self.iteration / self.tau1))
            self.learning_rate = self.base_learning * np.exp(-self.iteration / self.tau2)

            self.iteration += 1

            k = 100

            if self.iteration % k == 0:
                self.save(self.save_dir)
                umatrix = self.build_umatrix()
                self.q_hex.put((
                    umatrix,
                    self.iteration,
                    join(self.save_dir, 'pics', "{:04d}".format(self.iteration // k) + '.png')
                ))
                if plot_clusters:
                    # xx, yy = self.grid_to_mat()
                    graph  = self.grid_to_graph()
                    self.q_clusters.put((graph, self.iteration))
        self.q_hex.put(None)
        if plot_clusters:
            self.q_clusters.put(None)

    def read_csv(self, filename, labels=False):
        import csv

        self.logger.debug('Reading dataset from ' + filename)

        data = []

        with open(filename, 'r') as f:
            self.classes = 0
            reader = csv.reader(f)
            for row in reader:
                if 'X_0' in row:
                    self.dims = len(row) - 1
                    continue
                try:
                    for c in range(len(row) - 1):
                        row[c] = float(row[c])
                    row[-1] = int(row[-1])
                    # for c in range(len(row)):
                    #     row[c] = float(row[c])
                    if row[-1] + 1 > self.classes:
                        self.classes = row[-1] + 1
                    self.dims = len(row) - 1
                    if not labels:
                        row.pop()
                except ValueError as e:
                    raise
                    continue

                data.append(np.array(row) / 10)
                if labels:
                    row[-1] = int(row[-1] * 10)

        self.logger.debug('Dataset loaded')

        return data

    def initialize(self, data):
        for r in range(self.size):
            for c in range(self.size):
                vec = self.take_one(data)
                self.grid[c][r].initialize(self.dims, vec)
        return

    def take_one(self, data):
        import random

        rand_index = random.randint(0, len(data) - 1)
        vec = data[rand_index]

        return vec

    def winner(self, vec, metric='manhatten'):
        """winner

        Find Best Matching Unit to vector

        Distance metrics:
        .......................................
        :     Name     :       Formula        :
        :..............:......................:
        : "euclidean"  : sqrt(sum((x - y)^2)) :
        : "manhattan"  : sum(|x - y|)         :
        : "chebyshev"  : max(|x - y|)         :
        :..............:......................:


        Parameters
        ----------
        vec : ndarray
            vector from training set
        """

        distances = {
            "euclidean": self.euclidean,
            "manhatten": self.manhatten,
            "chebyshev": self.chebyshev
        }

        min_dist = None

        for r in range(self.size):
            for c in range(self.size):
                # print(vec, self.grid[c][r].weights)
                dist = distances[metric](vec, self.grid[c][r].weights)
                if min_dist is None or dist < min_dist:
                    min_dist  = dist
                    candidate = (c, r)

        winner = self.grid[candidate[0]][candidate[1]]
        winner.win()

        return winner

    def euclidean(self, vec1, vec2):
        from scipy.spatial.distance import euclidean
        return euclidean(vec1, vec2)

    def manhatten(self, vec1, vec2):
        from scipy.spatial.distance import cityblock
        return cityblock(vec1, vec2)

    def chebyshev(self, vec1, vec2):
        from scipy.spatial.distance import chebyshev
        return chebyshev(vec1, vec2)

    def update_weights(self, winner, vec, sigma, lr, metric='manhatten'):
        distances = {
            "euclidean": self.euclidean,
            "manhatten": self.manhatten,
            "chebyshev": self.chebyshev
        }
        for r in range(self.size):
            for c in range(self.size):
                dist = distances[metric](winner.coordinates, self.grid[c][r].coordinates)

                # print(dist)
                h = np.exp(-(dist ** 2) / (2 * sigma ** 2))

                delta_vec = vec - self.grid[c][r].weights
                delta_vec = delta_vec * lr * h

                self.grid[c][r].weights = self.grid[c][r].weights + delta_vec
        pass

    def build_umatrix(self, metric='euclidean'):
        distances = {
            "euclidean": self.euclidean,
            "manhatten": self.manhatten,
            "chebyshev": self.chebyshev
        }
        umatrix = []
        size = self.size * 2 - 1
        for r in range(size):
            column = []
            for c in range(size):
                if (r % 2 == 0) and (c % 2 == 0):
                    column.append(self.grid[c // 2][r // 2].weights)
                else:
                    column.append(0)
            umatrix.append(column.copy())

        max_dist = 0
        for r in range(size):
            for c in range(size):
                if (r % 2 == 0) and (c % 2 == 1):
                    dist = distances[metric](umatrix[c - 1][r], umatrix[c + 1][r])
                    # print(dist)
                    if dist > max_dist:
                        max_dist = dist

                    umatrix[c][r] = dist

                elif (r % 2 == 1) and (c % 2 == 0):
                    dist = distances[metric](umatrix[c][r - 1], umatrix[c][r + 1])
                    if dist > max_dist:
                        max_dist = dist

                    umatrix[c][r] = dist

        for r in range(size):
            for c in range(size):
                if (r % 2 == 0) and (c % 2 == 0):
                    avg = 0
                    cnt = 0
                    if r >= 1:
                        avg += (umatrix[c][r - 1])
                        cnt += 1
                    if r <= size - 2:
                        avg += (umatrix[c][r + 1])
                        cnt += 1
                    if c >= 1:
                        avg += (umatrix[c - 1][r])
                        cnt += 1
                    if c <= size - 2:
                        avg += (umatrix[c + 1][r])
                        cnt += 1

                    avg = avg / cnt
                    # print(avg, max_dist)
                    umatrix[c][r] = avg
                elif (r % 2 == 1) and (c % 2 == 1):
                    avg = 0
                    cnt = 0
                    if r >= 1:
                        avg += (umatrix[c][r - 1])
                        cnt += 1
                    if r <= size - 2:
                        avg += (umatrix[c][r + 1])
                        cnt += 1
                    if c >= 1:
                        avg += (umatrix[c - 1][r])
                        cnt += 1
                    if c <= size - 2:
                        avg += (umatrix[c + 1][r])
                        cnt += 1

                    avg = avg / cnt
                    # print(avg, max_dist)
                    umatrix[c][r] = avg

        # for r in range(size):
        #     for c in range(size):

        #             avg = avg / cnt

        #             umatrix[c][r] = avg

        for r in range(size):
            for c in range(size):
                umatrix[c][r] = 255 - int(255 * umatrix[c][r] / max_dist)

        return umatrix

    def save(self, directory):
        from os.path import join
        from utils import dict_to_json

        self.dict = {
            'size'          : self.size,
            'data_path'     : self.data_path,
            'max_iter'      : self.max_iter,
            'epoch'         : self.iteration,
            'sigma'         : self.sigma,
            'base_sigma'    : self.base_sigma,
            'base_learning' : self.base_learning,
            'learning_rate' : self.learning_rate,
            'minsigma'      : self.minsigma,
            'minlr'         : self.minlr,
            'tau1'          : self.tau1,
            'tau2'          : self.tau2,

            'grid'          : {}
        }

        for r in range(self.size):
            for c in range(self.size):
                self.dict['grid'][r * self.size + c] = {
                    'coordinates'   : [
                        int(self.grid[c][r].coordinates[0]),
                        int(self.grid[c][r].coordinates[1])
                    ],
                    'wins'          : self.grid[c][r].wins,
                    'weights'       : list(self.grid[c][r].weights)
                }

        dict_to_json(self.dict, join(directory, 'checkpoint.json'))

        return

    def load(self, path):
        from numpy import array
        from utils import json_to_dict

        self.dict = json_to_dict(path)

        self.size           = self.dict['size']
        self.max_iter      = self.dict['max_iter']
        self.iteration          = self.dict['epoch']
        self.sigma          = self.dict['sigma']
        self.base_sigma     = self.dict['base_sigma']
        self.base_learning  = self.dict['base_learning']
        self.learning_rate  = self.dict['learning_rate']
        self.minsigma       = self.dict['minsigma']
        self.minlr          = self.dict['minlr']
        self.tau1           = self.dict['tau1']
        self.tau2           = self.dict['tau2']
        self.data_path      = self.dict['data_path']

        self.grid = self.create_grid(self.size)

        for r in range(self.size):
            for c in range(self.size):
                cur = self.dict['grid'][str(r * self.size + c)]
                self.grid[c][r].coordinates = array(cur['coordinates'])
                self.grid[c][r].weights     = array(cur['weights'])
                self.grid[c][r].wins         = cur['wins']

    def grid_to_mat(self):
        xx = np.zeros((self.size, self.size))
        yy = np.zeros((self.size, self.size))
        for r in range(self.size):
            for c in range(self.size):
                xx[c][r] = self.grid[c][r].weights[0]
                yy[c][r] = self.grid[c][r].weights[1]
        return xx, yy

    def grid_to_graph(self):
        import networkx as nx

        G = nx.Graph()

        for r in range(self.size):
            for c in range(self.size):
                G.add_node(
                    (c, r),
                    pos=(
                        self.grid[c][r].weights[0],
                        self.grid[c][r].weights[1]
                    )
                )
                if r:
                    G.add_edge((c, r), (c, r - 1))
                if c:
                    G.add_edge((c, r), (c - 1, r))
        return G


def test():
    s = SOM(20)
    s.train('../MLP/datasets/3_classes/train.csv')
    return


def wdbc():
    s = SOM(20)
    s.train('../datasets/wdbc_normalized.csv')
    return


def wine():
    s = SOM(100)
    s.train('../datasets/wine_normalized.csv')
    return


def continuse_test():
    s = SOM(from_file='./checkpoints/som_1554925569/checkpoint.json')
    s.train(continue_train=True)
    return


if __name__ == '__main__':
    wine()
