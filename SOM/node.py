import numpy as np


class Node(object):
    """docstring for Node"""

    def __init__(self, coordinates):
        self.weights = []

        self.wins = 0

        self.coordinates = np.array(coordinates)

        return

    def initialize(self, dims, vec):
        # import random

        # for d in range(dims):
        #     self.weights.append(random.random() * 2 - 1)  # [-1, 1]

        # self.weights = np.array(self.weights)

        self.weights = vec
        return

    def win(self):
        self.wins += 1
