"""Some math and misc utils."""
import logging as lg
import numpy as np
import pandas as pd
import coloredlogs


def get_colored_logger(name: str, level: str = "INFO") -> lg.Logger:
    """Return a logger with a default ColoredFormatter.

    Levels:
    NOTSET->DEBUG->INFO->WARNING->ERROR->CRITICAL

    Args:
        name (str): Logger name
        level (Optional[str], optional): logging level, defaults to INFO

    Returns:
        logging.Logger: Description

    """

    logger = lg.getLogger(name)
    level = level.upper()

    if level == "INFO":
        logger.setLevel(lg.INFO)
    elif level == "DEBUG":
        logger.setLevel(lg.DEBUG)
    elif level == "WARNING":
        logger.setLevel(lg.WARNING)
    elif level == "ERROR":
        logger.setLevel(lg.ERROR)
    elif level == "CRITICAL":
        logger.setLevel(lg.CRITICAL)
    else:
        raise Exception("No such logging level")

    coloredlogs.install(level=level)

    return logger


def train_test_split(
        dataset: pd.DataFrame,
        ratio: float = 0.3
) -> tuple:
    """Split dataset on train and test."""

    mask = np.random.rand(len(dataset)) < 1 - ratio

    train = dataset[mask]
    test = dataset[~mask]

    return train, test
