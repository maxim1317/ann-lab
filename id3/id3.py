"""ID3 decision tree."""
from typing import Union
import json
from pathlib import Path
import numpy as np
import pandas as pd
from graphviz import Digraph

from .id3_utils import get_colored_logger, train_test_split


class ID3:
    """ID3 decision tree."""

    def __init__(self):
        self.tree: dict = {}
        self.target_feature: str = ''

        self._dataset: pd.DataFrame = None
        self._features: list = []
        self._logger = get_colored_logger('ID3', 'INFO')

    def _entropy(self, target_col: pd.Series) -> float:
        """Calculate the entropy of a dataset."""

        _, counts = np.unique(target_col, return_counts=True)
        entropy = np.sum(
            [
                (-count / np.sum(counts)) * np.log2(count / np.sum(counts))
                for count in counts
            ]
        )
        # self._logger.error(entropy)
        return entropy

    def _info_gain(self, data: pd.DataFrame, split_feature_name: str) -> float:
        """Calculate the information gain of a dataset.

        Args:
            data (pd.DataFrame): Dataset to calculate info gain on.
            split_feature_name (str): Feature to calculate info gain for.

        Returns:
            float: like Titanic
        """
        # Calculating whole entropy
        total_entropy = self._entropy(data[self.target_feature])

        # Finding unique values and number of their repetitions
        vals, counts = np.unique(data[split_feature_name], return_counts=True)

        # Calculating the weighted entropy
        weighted_entropy = np.sum(
            [
                (counts[i] / np.sum(counts)) * self._entropy(
                    data.where(data[split_feature_name] == vals[i]).dropna()[self.target_feature]
                ) for i in range(len(vals))
            ]
        )

        information_gain = total_entropy - weighted_entropy
        return information_gain

    def train(self, dataset: pd.DataFrame, target_feature: str = 'class'):
        """Train id3.

        Old proverb says:
        > Every man should should plant a tree, build a house and raise a son.

        This one is for building the tree.
        """
        self._logger.info('Starting training')
        self._dataset = dataset
        self.target_feature = target_feature
        self._logger.debug(self._dataset.columns)
        self.tree = self._id3(
            data=self._dataset,
            features=list(self._dataset.columns[:-1])
        )
        self._logger.info('Training succeeded')

    def _id3(
            self,
            data: pd.DataFrame,
            features: pd.Series,
            parent_node_class: Union[str, int] = None
    ) -> Union[str, int, dict]:
        """Run id3 algorithm.

        Args:
            data (pd.DataFrame): Dataset to run id3 on.
                On the first run equals original dataframe.
            features (pd.Series): Just passing names of features for recursive stuff.
            parent_node_class (str): Saving father's name to return to

        Returns:
            its comlicated

        """
        # self._logger.debug('Entering id3 with features: %s', features)
        # Found the leaf - return it
        if len(np.unique(data[self.target_feature])) <= 1:
            self._logger.debug('Found leaf: %s', int(np.unique(data[self.target_feature])[0]))
            return int(np.unique(data[self.target_feature])[0])

        # If the dataset is empty
        # return the best feature of original dataset
        if data.empty:
            full_data_best_feature_index = np.argmax(
                np.unique(self._dataset[self.target_feature], return_counts=True)[1]
            )
            self._logger.debug(
                "No data, %s", np.unique(
                    self._dataset[self.target_feature]
                )[full_data_best_feature_index]
            )
            return int(
                np.unique(
                    self._dataset[self.target_feature]
                )[full_data_best_feature_index]
            )

        # Go back!
        if not features:
            if isinstance(parent_node_class, (np.float64, np.int64)):
                parent_node_class = int(parent_node_class)
            self._logger.debug('No features, going back to %s', parent_node_class)
            if parent_node_class is not None:
                return parent_node_class
            else:
                raise Exception(
                    "Finally, someone let me out of my cage\n"
                    "Now time for me is nothing 'cause I'm counting no age\n"
                    "Now I couldn't be there\n"
                    "Now you shouldn't be scared\n"
                )

        # If none of the above holds true, grow the tree!

        # Set the default value for this node --> The mode target feature value of the current node
        parent_node_class = np.unique(
            data[self.target_feature]
        )[
            np.argmax(
                np.unique(data[self.target_feature], return_counts=True)[1]
            )
        ]

        # Select the best feature for dataset splitting
        item_values = [
            self._info_gain(
                data=data,
                split_feature_name=feature
            ) for feature in features
        ]
        best_feature = features[np.argmax(item_values)]
        self._logger.debug("Best feature: %s", best_feature)

        # Create root/subroot for our little tree
        tree: dict = {best_feature: {}}

        # Remove the feature with the best inforamtion gain from the feature space
        features.remove(best_feature)

        # Split dataset by selected feature and build a subtree for both branches

        thresh = data[best_feature].mean()
        sub_data = data.where(data[best_feature] <= thresh).dropna()

        # Recursion, here we go
        subtree = self._id3(
            data=sub_data,
            features=features,
            parent_node_class=parent_node_class
        )

        tree[best_feature]["value"] = thresh

        # Add the sub tree, grown from the sub_dataset to the tree under the root node
        tree[best_feature]["<"] = subtree

        sub_data = data.where(data[best_feature] > thresh).dropna()

        # Recursion, here we go
        subtree = self._id3(
            data=sub_data,
            features=features,
            parent_node_class=parent_node_class
        )

        # Add the sub tree, grown from the sub_dataset to the tree under the root node
        tree[best_feature][">="] = subtree

        self._logger.debug("tree: %s", tree)
        return tree

    def predict(self, query):
        """Predict one point."""
        # self._logger.error(query)
        tree = self.tree
        return self._predict(query, tree)

    def _predict(self, query, tree):
        if isinstance(tree, int):
            return tree
        split_feature = list(tree.keys())[0]
        value = query[split_feature]
        split_value = tree[split_feature]["value"]

        if value < split_value:
            tree = tree[split_feature]["<"]
        else:
            tree = tree[split_feature][">="]
        return self._predict(query, tree)

    def test(self, data):
        """Test id3 on testing data."""
        queries = data.iloc[:, :-1].to_dict(orient="records")

        predicted = pd.DataFrame(columns=["predicted"])

        for i in range(len(data)):
            predict = self.predict(queries[i])
            # self._logger.error(predict)
            predicted = predicted.append(
                {'predicted': predict}, ignore_index=True
            )

        # self._logger.error(predicted.describe())

        accuracy = np.sum(
            np.where(
                predicted["predicted"].values == data["class"].values,
                True, False
            )
        ) / len(data)

        message = 'The prediction accuracy is: {}%'.format(accuracy * 100)
        self._logger.warning(message)

        return accuracy

    def as_json(self, save_to: str = None) -> str:
        """Print or save tree as json."""
        js_tree = json.dumps(self.tree, indent=4)
        message = (
            "\n"
            "============\n"
            "  ID3 tree\n"
            "============\n"
            f"{js_tree}"
        )

        if save_to is not None:
            self._logger.debug(message)

            path = Path(save_to)
            with path.open('w+') as file:
                file.write(js_tree)

        else:
            self._logger.warning(message)

        return js_tree

    def from_json(self, load_from: str) -> dict:
        """Load tree from json savefile."""
        path = Path(load_from)

        with path.open('r') as file:
            tree = json.load(file)

        self.tree = tree
        self.as_json()

        return tree

    def to_dot(self, save_to: str, format: str = 'png'):
        path = Path(save_to)
        graph = Digraph(name='ID3 Tree', format=format)

        tree = self.tree
        graph = self._to_dot(
            tree,
            graph
        )
        graph.render(path)

    def _to_dot(
            self,
            tree: Union[dict, int],
            graph: Digraph,
            parent: Union[str, int] = None,
            label: str = None
    ) -> Union[dict, int]:
        if isinstance(tree, int):

            if parent:
                _id = parent + str(tree)
                graph.node(_id, label=str(tree))
                graph.edge(parent, _id, label=label)
            return graph
        split_feature = list(tree.keys())[0]
        split_value = tree[split_feature]["value"]

        node = (
            f"{split_feature} < {split_value:.2}"
        )
        graph.node(node, shape='diamond')
        if parent:
            graph.edge(parent, node, label=label)

        subtree = tree[split_feature]["<"]
        graph = self._to_dot(
            subtree,
            graph,
            node,
            r'<'
        )

        subtree = tree[split_feature][">="]
        graph = self._to_dot(
            subtree,
            graph,
            node,
            r'⩾'
        )

        return graph


def run():
    """Run id3."""
    savefile = 'id3.json'

    dataset = pd.read_csv(Path('./datasets/banknote_authentication.csv'))
    train, test = train_test_split(dataset=dataset, ratio=0.3)

    id3 = ID3()
    try:
        id3.from_json(load_from=savefile)
    except Exception:
        id3.train(dataset=train)
        id3.as_json(save_to='id3.json')
    id3.test(test)
    id3.to_dot('id3.gv')
