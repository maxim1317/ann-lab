#! /bin/bash

#
# Testing using pytest
#

source scripts/colors.sh
. ./scripts/utils.sh

TESTNAME=pytest
FLAG_NOCAPTURE=-s
FLAG_VERBOSE=-v
FLAG_COVERAGE=--cov

testing $TESTNAME

check_input "$@"
py_lib_check $TESTNAME
py_lib_check $TESTNAME-cov
py_lib_check $TESTNAME-sugar

PROJECT_NAME=$1

FLAG_COVERAGE_FULL="${FLAG_COVERAGE} ${PROJECT_NAME}"

$TESTNAME $FLAG_NOCAPTURE $FLAG_VERBOSE $FLAG_COVERAGE_FULL $PROJECT_NAME && ok $TESTNAME || fail $TESTNAME
coverage html -d docs/ && exit 0 || exit 1