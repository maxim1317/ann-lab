# from datetime import datetime
# from matplotlib import pyplot
# from matplotlib.animation import FuncAnimation
# from random import randrange

# x_data, y_data = [], []

# pyplot.xkcd()

# figure = pyplot.figure()
# line, = pyplot.plot_date(x_data, y_data, '-')

# last = 1


# def update(frame):
#     x_data.append(last)
#     y_data.append(randrange(0, 100))
#     line.set_data(x_data, y_data)
#     figure.gca().relim()
#     figure.gca().autoscale_view()
#     return line


# animation = FuncAnimation(figure, update, interval=50)

# pyplot.show()


import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0, 10 * np.pi, 100)
y = np.sin(x)

plt.ion()
fig = plt.figure()
ax = fig.add_subplot(111)
line1, = ax.plot(x, y, 'b-')

for phase in np.linspace(0, 10 * np.pi, 1000):
    line1.set_ydata(np.sin(0.5 * x + phase))
    fig.gca().relim()
    fig.gca().autoscale_view()
    fig.canvas.draw()