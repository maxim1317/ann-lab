from neuron import Neuron
from activators import activators, derivatives


class Layer(object):
    """Father class for Layers"""

    def __init__(self, neurons_num, previous):
        self.neurons_num = neurons_num
        self.previous    = previous

        self.neurons = {}

        for neuron in range(neurons_num):
            self.neurons[neuron] = Neuron(
                _id=neuron,
                prev_layer=self.previous,
                activator=self.activator,
                derivative=self.derivative,
            )

    def think(self):
        for neuron in self.neurons.values():
            neuron.think()

    def initialize(self):
        for neuron in self.neurons.values():
            neuron.initialize()

    def get_layer_structure(self):
        structure = {
            'type'       : self.name,
            'neurons_num': self.neurons_num,
            'neurons'    : {}
        }

        for l in range(len(self.neurons)):
            structure['neurons'][l] = self.neurons[l].get_neuron_structure()

        return structure

    def load(self, data):
        for i, n in data['neurons'].items():
            self.neurons[int(i)].load(n)
        return

    def console(self):
        console  = ''
        console += self.name + '\n'
        console += '    Number of neurons: ' + str(self.neurons_num) + '\n'
        for neuron in self.neurons.values():
            console += neuron.console()
        return console


class ReLU(Layer):
    """ReLU Layer"""
    name = 'ReLU'
    activator  = activators['ReLU']
    derivative = derivatives['ReLU']


class Linear(Layer):
    """Linear Layer"""
    name = 'Linear'
    activator  = activators['Linear']
    derivative = derivatives['Linear']


class Sigmoid(Layer):
    """Sigmoid Layer"""
    name = 'Sigmoid'
    activator  = activators['Sigmoid']
    derivative = derivatives['Sigmoid']


class Tanh(Layer):
    """Tanh Layer"""
    name = 'Tanh'
    activator  = activators['Tanh']
    derivative = derivatives['Tanh']


class Input(Layer):
    """Input Layer"""
    name = 'Input'
    previous = None
    activator  = activators['Linear']
    derivative = derivatives['Linear']


Layers = {
    'ReLU'   : ReLU,
    'Linear' : Linear,
    'Sigmoid': Sigmoid,
    'Tanh'   : Tanh,
    'Input'  : Input
}