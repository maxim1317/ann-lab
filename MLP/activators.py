def fn_ReLU(self, x):
    return max(0, x)


def fn_ReLU_der(self, x):
    return int(x > 0)


def fn_linear(self, x):
    return x


def fn_linear_der(self, x):
    return 0


def fn_sigmoid(self, x):
    from numpy import exp

    return 1 / (1 + exp(-x))


def fn_sigmoid_der(self, x):
    return x * (1 - x)


def fn_tanh(self, x):
    from numpy import tanh

    return tanh(x)


def fn_tanh_der(self, x):
    return 1 - x ** 2


def fn_softmax(self, X):
    """Compute softmax values for each sets of scores in X."""
    import numpy as np
    e_x = np.exp(X - np.max(X))
    return e_x / e_x.sum()


def fn_softmax_der(self, x):
    """TODO"""
    return 0


# def fn_step(self, x):
#     return int(x > 0)


activators = {
    'ReLU'   : fn_ReLU,
    'Linear' : fn_linear,
    'Sigmoid': fn_sigmoid,
    'Tanh'   : fn_tanh,
    'Softmax': fn_softmax,
    # 'Step'   : fn_step
}

derivatives = {
    'ReLU'   : fn_ReLU_der,
    'Linear' : fn_linear_der,
    'Sigmoid': fn_sigmoid_der,
    'Tanh'   : fn_tanh_der,
    'Softmax': fn_softmax_der,
    # 'Step'   : fn_step_der
}
