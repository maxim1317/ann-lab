def train(self, train_path=None, val_path=None, plot=False, continue_train=False, max_epoch=1000):
    import time
    from os.path import join
    import multiprocessing
    # from queue import Queue
    from plotter import plotter, mapper
    from utils import ensure_dir

    self.max_epoch = max_epoch

    min_loss = 1

    if train_path is None and not continue_train:
        self.logger.warning('Nothing to train on')
        return

    if not continue_train:
        self.logger.info('Training started')
        self.logger.debug(self.console())

        self.trained_on = train_path

        self.initialize()
        self.epoch = 0

        train_path = self.trained_on
        train_data = self.read_csv(train_path, check=True)
    else:
        train_path = self.trained_on
        train_data = self.read_csv(train_path, check=False)
    # print(continue_train)
    # print(self.inputs)

    if len(train_data[0]) - 1 != 2:
        plot = False

    if val_path is not None:
        val_flag = True
        val_data = self.read_csv(val_path)
    else:
        val_flag = False

    # print(train_data)

    self.save_dir = join('checkpoints', 'mlp_' + str(int(time.time())))
    ensure_dir(self.save_dir)

    self.q_train = multiprocessing.Queue()
    p = multiprocessing.Process(target=plotter, args=(self.q_train, 'Train'))
    p.start()

    if val_flag:
        self.q_val = multiprocessing.Queue()
        v = multiprocessing.Process(target=plotter, args=(self.q_val, 'Validation'))
        v.start()
    if plot:
        self.q_map = multiprocessing.Queue()
        m = multiprocessing.Process(target=mapper, args=(self.q_map, train_data))
        m.start()

    while self.epoch <= max_epoch:
        self.logger.info('Epoch ' + str(self.epoch))
        batch, targets = self.shuffle(train_data)
        # print(batch)

        avg = 0

        for point in range(len(batch)):
            prediction = (self.forward_propagation(batch[point]))

            # print(predictions)
            self.train_loss = self.calculate_loss(prediction, targets[point])
            avg += self.train_loss
            # print(loss)
            self.back_propagation(prediction, targets[point])
            # break
        avg = avg / len(batch)
        self.q_train.put((self.epoch, avg))
        self.logger.debug('Train loss      = ' + str(avg))

        if val_flag:
            self.val_loss = self.validate(val_data)

            if self.val_loss < min_loss:
                min_loss = self.val_loss
                self.save(self.save_dir)
            self.q_val.put((self.epoch, self.val_loss))

        if plot and (self.epoch % 50 == 0):
            mesh_predicted = self.get_mesh()
            self.q_map.put((self.pred_dots, mesh_predicted, self.epoch))
        self.logger.debug('Validation loss = ' + str(self.val_loss))

        self.epoch += 1
        # break
    self.q_train.put(None)
    if val_flag:
        self.q_val.put(None)
        if plot:
            self.q_map.put(None)


def get_mesh(self):
    import numpy as np
    from plotter import get_grid

    xx, yy = get_grid(border=16)

    cc = np.zeros_like(xx)

    for x in range(xx.shape[0]):
        for y in range(yy.shape[0]):
            cc[x][y] = self.decode(self.inference([xx[x][y], yy[x][y]]))

    return (xx, yy, cc)


def back_propagation(self, prediction, target, ldtype='L2'):

    #####################################################
    #                                                   #
    #    inputs[0]                                      #
    #    +-------+ weights[0]    neuron.          +---+ #
    # +-->sum|out+------+     +-----------+   +---> | | #
    #    +-------+      +----->     |     +---+   +---+ #
    #                         | sum | out |             #
    #    +-------+      +----->     |     +---+   +---+ #
    # +-->sum|out+------+     +-----------+   +---> | | #
    #    +-------+ weights[1]                     +---+ #
    #    inputs[1]                                      #
    #                                                   #
    #####################################################

    from loss import losses_der

    # Working with output layer

    cur_layer      = self.layers[len(self.layers) - 1]  # Selecting layer
    loss_der       = losses_der[ldtype]                 # loading derivative of loss function
    activation_der = cur_layer.derivative               # loading derivative of activation function

    # dE_dO = loss_der(np.array(prediction), np.array(target))  # Calculating dE/dO

    # OUTER LAYER
    for k, neuron in cur_layer.neurons.items():

        neuron.dE_dO = loss_der(prediction[k], self.encode(target)[k])
        neuron.dO_dS = activation_der(neuron.out)
        neuron.gamma = neuron.dE_dO * neuron.dO_dS

        neuron.dE_dW = {}

        for i in range(len(neuron.weights)):
            neuron.dS_dW = neuron.inputs[i].out

            neuron.dE_dW[i] = neuron.dE_dO * neuron.dO_dS * neuron.dS_dW

    # INNER LAYERS
    # ......................................................
    # : dE/dW : dE/dO * dO/dS * dS/dW                      :
    # ......................................................
    # : dS/dW : neuron.inputs[i].out                       :
    # : dO/dS : activation_der(neuron.sum)                 :
    # : dE/dO : sum(neuron_n+1.gamma * neuron_n+1.weights) :
    # :.......:............................................:
    for i in range(len(self.layers) - 2, 0, -1):
        # print(i, '#############################')
        cur_layer      = self.layers[i]        # Selecting layer
        next_layer     = self.layers[i + 1]    #
        loss_der       = losses_der[ldtype]    # loading derivative of loss function
        activation_der = cur_layer.derivative  # loading derivative of activation function

        for k, neuron in cur_layer.neurons.items():
            neuron.dE_dO = 0
            for k_next, neuron_next in next_layer.neurons.items():
                neuron.dE_dO += neuron_next.gamma * neuron_next.weights[k]

            neuron.dO_dS = activation_der(neuron.out)

            neuron.gamma = neuron.dE_dO * neuron.dO_dS

            neuron.dE_dW = {}

            for i in range(len(neuron.weights)):
                neuron.dS_dW = neuron.inputs[i].out

                neuron.dE_dW[i] = neuron.dE_dO * neuron.dO_dS * neuron.dS_dW
                # neuron.inputs[i].delta += neuron.dE_dW

    # UPDATING WEIGHTS
    for i in range(len(self.layers) - 1, 0, -1):
        # print('###################  #############', i)
        cur_layer = self.layers[i]
        for k, neuron in cur_layer.neurons.items():
            for s in range(len(neuron.weights)):
                neuron.weights[s] -= neuron.dE_dW[s] * self.learning_rate
            neuron.bias -= neuron.gamma * neuron.bias * self.learning_rate

    return


def forward_propagation(self, point):
    import numpy as np

    np.seterr(over='ignore')

    # print(self.layers[0].neurons)
    for i, neuron in self.layers[0].neurons.items():
        neuron.out = point[i]

    for l in range(1, len(self.layers)):
        self.layers[l].think()

    out = []
    last = self.layers[len(self.layers) - 1]

    for neuron in last.neurons.values():
        out.append(neuron.out)

    return out


def validate(self, val_data):
    batch, targets = self.shuffle(val_data)

    avg = 0

    stats = {
        'True' : 0,
        'False': 0
    }

    self.pred_dots = batch.copy()

    for point in range(len(batch)):
        prediction = (self.forward_propagation(batch[point]))
        # print('\n', self.decode(prediction), targets[point])
        if self.decode(prediction) != targets[point]:
            stats['False'] += 1
        else:
            stats['True']  += 1

        self.pred_dots[point].append(self.decode(prediction))

        val_loss = self.calculate_loss(prediction, targets[point])
        avg += val_loss

    accuracy = stats['True'] / len(batch)

    self.logger.debug('Correct   = ' + str(stats['True']))
    self.logger.debug('Incorrect = ' + str(stats['False']))
    self.logger.debug('Accuracy  = ' + str(accuracy))

    avg = avg / len(batch)
    return avg


def calculate_loss(self, prediction, target, ltype='L2'):
    import numpy as np
    from loss import losses

    # self.logger.debug('Calculating "' + ltype + '" loss function')
    # print('#################################', self.encode(target))
    loss  = losses[ltype](np.array(prediction), np.array(self.encode(target)))
    total = np.sum(loss)
    # print('Loss for prediction', prediction, target, total)

    return total


def encode(self, num):
    bits  = self.classes
    length = self.outputs
    step   = length / bits

    output_arr = [0] * length
    # print('output_arr', output_arr)

    fr = step * num
    to = step * (num + 1)

    # print('From', fr, 'To:', to)

    point = (to - fr) / 2 + fr

    for o in range(len(output_arr)):
        if not (o <= fr < o + 1) and not (o < to <= o + 1):
            continue
        else:
            if point >= o + 1:
                output_arr[o] = 1.0
            elif point <= o:
                output_arr[o] = 0.0
            else:
                output_arr[o] = point - o

    # print('Encoded', num, 'to', output_arr)

    return output_arr


def decode(self, arr):
    bits  = self.classes
    length = 1 * self.outputs
    step   = length / bits

    centers = []
    for b in range(bits):
        fr = step * b
        to = step * (b + 1)

        center = (to - fr) / 2 + fr
        centers.append(center)

    nums  = []
    dists = []

    # print(arr)

    for point in arr:
        num, dist = get_closest(point + arr.index(point) * step, centers)
        nums.append(num)
        dists.append(dist)

    num = nums[dists.index(min(dists))]
    # num = dists.index(min(dists))
    # print('Decoded', arr, 'as', num, nums)
    return num


def get_closest(point, centers):
    min_dist = 100
    min_num  = 0

    # dists = []/
    # print('centers', centers)
    for c in centers:
        dist = abs(c - point)

        # dists.append(dist)
        if dist <= min_dist:
            min_dist = dist
            min_num = centers.index(c)

    # print(dists)
    return min_num, min_dist


def shuffle(self, data, batch_size=0):
    import random as r
    from copy import deepcopy

    # def bitfield(n):
    #     return [int(digit) for digit in bin(n)[2:]]

    # self.logger.debug('Shuffling batch')

    if not batch_size:
        batch_size = len(data)
        batch = deepcopy(data)
    else:
        batch = deepcopy(data)[:batch_size]

    r.shuffle(batch)

    targets = []
    for b in batch:
        targets.append(b.pop())

    # self.logger.debug('Batch shuffled')
    # print(targets)

    return batch, targets


def read_csv(self, filename, check=True):
    import csv

    self.logger.debug('Reading dataset from ' + filename)

    data = []

    with open(filename, 'r') as f:
        self.classes = 0
        reader = csv.reader(f)
        for row in reader:
            if 'X_0' in row:
                continue
            try:
                for c in range(len(row) - 1):
                    row[c] = float(row[c])
                row[-1] = int(row[-1])
                if row[-1] + 1 > self.classes:
                    self.classes = row[-1] + 1
                if self.inputs != len(row) - 1 and check:
                    self.logger.info('Corrected amount of inputs from ' + str(self.inputs) + ' to ' + str(len(row) - 1))
                    self.inputs = len(row) - 1
                    self.initialize()
            except ValueError as e:
                raise
                continue

            data.append(row.copy())

    self.logger.debug('Dataset loaded')

    return data


def to_csv(self, filename, points):
    import csv
    labels = []

    for dim in range(self.inputs):
        labels.append('X_' + str(dim))

    labels.append('cluster')

    with open(filename, 'w') as f:
        w = csv.writer(f)

        w.writerow(labels)

        for line in range(len(points)):
            w.writerow(points[line])


if __name__ == '__main__':
    from perceptron import test2
    test2()
