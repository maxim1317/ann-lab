def plotter(q, name):
    import logging
    import matplotlib.pyplot as plt

    logging.getLogger("matplotlib").setLevel(logging.WARNING)

    fig = plt.gcf()
    fig.show()
    fig.canvas.draw()

    x = []
    y = []
    plt.plot(x, y)
    while True:
        from_q = q.get()
        if from_q is None:
            break
        plt.clf()

        plt.title(name)
        x.append(from_q[0])
        y.append(from_q[1])
        plt.plot(x, y)
        plt.draw()
        fig.canvas.draw()
        plt.pause(0.01)

    plt.show()
    return


def mapper(q, tests):
    import logging
    import matplotlib.pyplot as plt

    logging.getLogger("matplotlib").setLevel(logging.WARNING)

    fig = plt.gcf()
    fig.show()
    fig.canvas.draw()

    # plt.xkcd()

    testx  = []
    testy  = []
    testsp = []
    testc  = []

    for test in tests:
        testx.append(test[0])
        testy.append(test[1])
        testsp.append(36)
        testc.append(test[2])
    # plt.scatter(dotx, doty, dotsp, c=tuple(dotc), marker='v', cmap=discrete_cmap(len(dotc) + 10, 'gist_rainbow'))
    while True:
        dotx  = []
        doty  = []
        dotsp = []
        dotc  = []

        got = q.get()

        if got is None:
            break
        plt.clf()

        dots, mesh, epoch = got

        plt.title('Epoch ' + str(epoch))

        for dot in dots:
            dotx.append(dot[0])
            doty.append(dot[1])
            dotsp.append(48)
            dotc.append(dot[2])

        xx, yy, cc = mesh
        plt.pcolormesh(xx, yy, cc, cmap='gist_rainbow')
        plt.scatter(testx, testy, testsp, c=tuple(testc), marker='o', cmap='gist_rainbow', edgecolors='white')
        plt.scatter(dotx, doty, dotsp, c=tuple(dotc), marker='s', cmap='gist_rainbow', edgecolors='white')
        # plt.scatter(testx, testy, testsp, c=tuple(testc), marker='o', cmap=discrete_cmap(len(testc) + 10, 'gist_rainbow'))
        # plt.scatter(dotx, doty, dotsp, c=tuple(dotc), marker='v', cmap=discrete_cmap(len(dotc) + 10, 'gist_rainbow'), edgecolors='black')
        plt.draw()
        fig.canvas.draw()
        plt.pause(0.01)

    plt.show()

    return


def discrete_cmap(N, base_cmap=None):
    import matplotlib.pyplot as plt
    import numpy as np
    """Create an N-bin discrete colormap from the specified input map"""

    # Note that if base_cmap is a string or None, you can simply do
    #    return plt.cm.get_cmap(base_cmap, N)
    # The following works for string, None, or a colormap instance:

    base = plt.cm.get_cmap(base_cmap)
    color_list = base(np.linspace(0, 1, N))
    cmap_name = base.name + str(N)
    return base.from_list(cmap_name, color_list, N)


def get_grid(X=None, Y=None, border=None):
    import numpy as np
    if border is not None:
        x_min = y_min = -border - 1
        x_max = y_max = border + 1
    else:
        x_min, x_max = min(X) - 1, max(X) + 1
        y_min, y_max = min(Y) - 1, max(Y) + 1
    return np.meshgrid(np.arange(x_min, x_max, 0.5), np.arange(y_min, y_max, 0.5))
