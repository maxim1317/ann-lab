class Perceptron(object):
    """docstring for Perceptron"""

    def __init__(self, inputs=0, outputs=0, learning_rate=0.3, from_file=None):
        import logging as lg
        import coloredlogs
        # import random

        # random.seed(1)

        self.logger = lg.getLogger('perceptron')
        self.logger.setLevel(lg.DEBUG)

        fh = lg.FileHandler('perceptron.log')
        fh.setLevel(lg.DEBUG)
        ch = lg.StreamHandler()
        ch.setLevel(lg.DEBUG)

        formatter = lg.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        # ch.setFormatter(formatter)

        self.logger.addHandler(fh)
        # self.logger.addHandler(ch)

        coloredlogs.install(level='DEBUG', logger=self.logger)

        if from_file is not None:
            self.load(from_file)
            return

        self.logger.info('Perceptron created')

        self.inputs  = inputs
        self.outputs = outputs

        self.learning_rate = learning_rate

        self.layers = {}

        self.train_loss = None
        self.val_loss   = None
        self.trained_on = None
        self.epoch = 0

        self.add_layer('Input', self.inputs)
        return

    def add_layer(self, ltype, neurons):
        from layers import Layers

        if ltype == 'Input':
            i = 0
            previous = None
        else:
            i = len(self.layers)
            previous = self.layers[i - 1]

        self.outputs = neurons

        self.layers[i] = Layers[ltype](neurons, previous)
        self.logger.info(ltype + ' layer added')
        self.logger.info('Structure: ' + self.get_structure_text())

    def initialize(self):
        self.logger.debug('Initializing weights')
        for l in range(1, len(self.layers)):
            self.layers[l].initialize()
        self.logger.debug('Weights initialized')
        return

    def inference(self, point):
        return self.forward_propagation(point)

    def save(self, location):
        from os.path import join

        from utils import dict_to_json

        checkpoint = {
            'epoch'         : self.epoch,
            'train_loss'    : self.train_loss,
            'val_loss'      : self.val_loss,
            'trained_on'    : self.trained_on,
            'inputs'        : self.inputs,
            'outputs'       : self.outputs,
            'learning_rate' : self.learning_rate,
            'layers'        : {}
        }
        for l in range(len(self.layers)):
            # print(self.layers[l].get_layer_structure())
            checkpoint['layers'][l] = self.layers[l].get_layer_structure()

        filename = 'checkpoint' + '.json'
        path = join(location, filename)

        dict_to_json(checkpoint, path)

        self.logger.info(
            'Saved perceptron at epoch ' + str(self.epoch) +
            ' with validation loss ' + str(self.val_loss)  +
            ' to "' + path + '"'
        )

        filename = 'predictions' + '.csv'
        path = join(location, filename)
        self.to_csv(path, self.pred_dots)

        return

    def load(self, filename):
        from utils import json_to_dict

        p_struct = json_to_dict(filename)

        self.inputs        = p_struct['inputs']
        self.outputs       = p_struct['outputs']

        self.learning_rate = p_struct['learning_rate']

        self.train_loss    = p_struct['train_loss']
        self.trained_on    = p_struct['trained_on']
        self.epoch         = p_struct['epoch']

        self.layers        = {}

        for n, l in p_struct['layers'].items():
            self.add_layer(ltype=l['type'], neurons=l['neurons_num'])
            self.layers[int(n)].load(l)

        return

    def encode(self, num):
        import learning
        return learning.encode(self, num)

    def decode(self, arr):
        import learning
        return learning.decode(self, arr)

    def train(self, train_path=None, val_path=None, plot=False, continue_train=False, max_epoch=1000):
        import learning
        return learning.train(self, train_path, val_path, plot, continue_train, max_epoch)

    def validate(self, val_data):
        import learning
        return learning.validate(self, val_data)

    def back_propagation(self, prediction, target, ldtype='L2'):
        import learning
        return learning.back_propagation(self, prediction, target, ldtype)

    def forward_propagation(self, point):
        import learning
        return learning.forward_propagation(self, point)

    def calculate_loss(self, prediction, target, ltype='L2'):
        import learning
        return learning.calculate_loss(self, prediction, target, ltype)

    def shuffle(self, data, batch_size=0):
        import learning
        return learning.shuffle(self, data, batch_size)

    def get_mesh(self):
        import learning
        return learning.get_mesh(self)

    def read_csv(self, filename, check=True):
        import learning
        return learning.read_csv(self, filename)

    def to_csv(self, filename, points):
        import learning
        return learning.to_csv(self, filename, points)

    def get_structure_text(self):
        structure = ''

        for l in self.layers.values():
            structure += '> ' + l.name + '(' + str(l.neurons_num) + ')' + ' >'

        return structure

    def console(self):
        console = '\n'
        console += 'Perceptron' + '\n'
        console += 'Structure: ' + self.get_structure_text() + '\n'
        console += 'Layers' + '\n'
        for layer in self.layers.values():
            console += layer.console() + '\n'
        return console


def p_3_classes(p):

    p.train(
        train_path='datasets/5_classes/train.csv',
        val_path='datasets/5_classes/test.csv',
        plot=True
    )


if __name__ == '__main__':
    p = Perceptron(2)
    p.add_layer('ReLU', 8)
    p.add_layer('ReLU', 8)
    p.add_layer('ReLU', 8)
    # p.add_layer('ReLU', 8)
    p.add_layer('Sigmoid', 1)

    p_3_classes(p)
