import random as r


class Neuron(object):
    """docstring for Neuron"""

    def __init__(
        self,
        _id,
        prev_layer,
        activator,
        derivative
    ):
        self.weights = {}

        self.id         = _id
        self.prev_layer = prev_layer
        self.activator  = activator
        self.derivative = derivative

        if prev_layer is not None:
            self.initialize()
        else:  # Input Layer
            self.inputs  = None
            self.weights = None

        self.sum = 0
        self.out = 0

        self.bias = 1

        self.gamma = 0
        self.delta = 0

    def think(self):
        self.sum = 0
        for i in range(len(self.inputs)):
            self.sum += self.inputs[i].out * self.weights[i]

        self.sum += self.bias

        self.out = self.activator(self.sum)

    def initialize(self):
        import math as m
        self.inputs = self.prev_layer.neurons
        for inp in self.inputs:
            self.weights[inp] = r.random() * m.sqrt(1 / len(self.inputs))

    def get_neuron_structure(self):
        structure = {
            'out'    : self.out,
            'gamma'  : self.gamma,
            'delta'  : self.delta,
            'sum'    : self.sum,
            'bias'   : self.bias,
            'weights': {}
        }
        if self.weights is not None:
            for l in range(len(self.weights)):
                structure['weights'][l] = self.weights[l]
        else:
            structure['weights'] = None
        return structure

    def load(self, data):
        if data['weights'] is not None:
            for i, w in data['weights'].items():
                self.weights[int(i)] = w
        else:
            self.weights = None

        self.sum   = data['sum']
        self.out   = data['out']

        self.bias  = data['bias']

        self.gamma = data['gamma']
        self.delta = data['delta']

    def console(self):
        prefix = '    '

        console = ''
        console += prefix + 'id    : ' + str(self.id)  + '\n'
        console += prefix + 'sum   : ' + str(self.sum) + '\n'
        console += prefix + 'out   : ' + str(self.out) + '\n'
        console += prefix + 'inputs: ' + '\n'

        prefix += '    '
        if self.weights is not None:
            for i, w in self.weights.items():
                console += prefix + str(i) + ': ' + str(self.inputs[i].out) + ' x ' + str(w) + '\n'
        else:
            console += prefix + 'None' + '\n'

        return console
