import numpy as np


def cross_entropy(predictions, targets, epsilon=1e-10):
    predictions = np.clip(predictions, epsilon, 1. - epsilon)

    N = predictions.shape[0]
    ce_loss = -np.sum(np.sum(targets * np.log(predictions + 1e-5))) / N

    return ce_loss


def L1(predictions, targets):
    return np.sum(np.absolute(predictions - targets))


def L2(prediction, target):
    return (prediction - target) ** 2 / 2


def L2_der(prediction, target):
    return prediction - target


losses = {
    'Cross entropy': cross_entropy,
    'L1'           : L1,
    'L2'           : L2
}

losses_der = {
    'L2': L2_der
}