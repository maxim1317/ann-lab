"""Some math and misc utils."""
import matplotlib.pyplot as plt
from typing import Union
import logging as lg
import numpy as np
import coloredlogs


def euclidean_distance(
        vector_1: np.ndarray,
        vector_2: np.ndarray
) -> np.ndarray:
    """Calculate euclidean distance."""
    vector_1 = np.array(vector_1)
    vector_2 = np.array(vector_2)

    return np.linalg.norm(vector_1 - vector_2)


def train_test_split(
        dataset: Union[list, np.ndarray],
        ratio: float = 0.3
) -> tuple:
    """Split dataset on train and test."""
    dataset = np.array(dataset)
    np.random.shuffle(dataset)

    # pylint: disable=unbalanced-tuple-unpacking
    # test, train = np.array_split(
    #     dataset,
    #     int(dataset.shape[0] * ratio),
    #     axis=0
    # )
    in_test = int(dataset.shape[0] * ratio)
    test, train = dataset[:in_test], dataset[in_test:]

    # print(test, train)
    return train, test


def get_colored_logger(name: str, level: str = "INFO") -> lg.Logger:
    """Return a logger with a default ColoredFormatter.

    Levels:
    NOTSET->DEBUG->INFO->WARNING->ERROR->CRITICAL

    Args:
        name (str): Logger name
        level (Optional[str], optional): logging level, defaults to INFO

    Returns:
        logging.Logger: Description

    """

    logger = lg.getLogger(name)
    level = level.upper()

    if level == "INFO":
        logger.setLevel(lg.INFO)
    elif level == "DEBUG":
        logger.setLevel(lg.DEBUG)
    elif level == "WARNING":
        logger.setLevel(lg.WARNING)
    elif level == "ERROR":
        logger.setLevel(lg.ERROR)
    elif level == "CRITICAL":
        logger.setLevel(lg.CRITICAL)
    else:
        raise Exception("No such logging level")

    coloredlogs.install(level=level)

    return logger


def draw_bar_chart(stats: dict, best_K: int):
    Ks = tuple(stats.keys())
    y_pos = np.arange(len(Ks))
    scores = list(stats.values())

    bars = plt.bar(y_pos, scores, align='center', alpha=0.5)
    bars[best_K - 1].set_color('r')
    plt.xticks(y_pos, Ks)
    plt.xlabel('K')
    plt.ylabel('Score')
    plt.title('KNN')

    plt.show()
