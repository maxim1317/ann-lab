"""K-Nearest Neighbors."""
import operator
from pathlib import Path
from typing import Union
import numpy as np
from tqdm import tqdm

from .knn_utils import (
    euclidean_distance,
    get_colored_logger,
    draw_bar_chart
)


class KNN:
    """K-Nearest Neighbors"""
    def __init__(self):
        self._logger = get_colored_logger('KNN', level='INFO')
        self._x_train: Union[list, np.array] = None
        self._y_train: Union[list, np.array] = None

    def train(self, train_set: Union[list, np.array]):
        """
        Train KNN.

        Well, not exactly train.
        Just save train dataset.
        """
        train = np.array(train_set)
        # pylint: disable=unbalanced-tuple-unpacking
        self._x_train, self._y_train = (
            train[:, :train.shape[1] - 1],
            train[:, train.shape[1] - 1:]
        )
        message = (
            "\n"
            "Train dataset statistics:\n"
            "------------------------\n"
            f"Records: {self._x_train.shape[0]}\n"
            f"Dimensions: {self._x_train.shape[1]}\n"
            f"Classes: {np.unique(self._y_train).shape[0]}"
        )
        self._logger.debug(message)

    def predict(self, point: Union[list, np.array], K: int) -> Union[int, str]:
        """Predict point class."""
        point = np.array(point)

        self._logger.debug("Predicting point %s using %d neighbors", point, K)

        dists = {}

        for i in range(len(self._x_train)):
            dists[i] = euclidean_distance(self._x_train[i], point)

        k_neighbors = sorted(dists, key=dists.get)[:K]
        # self._logger.debug(k_neighbors)

        labels: dict = {}
        for k_neighbor in k_neighbors:
            if labels.get(k_neighbor, False):
                labels[int(self._y_train[k_neighbor])] += 1
            else:
                labels[int(self._y_train[k_neighbor])] = 1

        predict = max(labels.items(), key=operator.itemgetter(1))[0]
        # self._logger.debug(predict)

        return predict

    def test(self, validation_set: Union[list, np.array], K: int) -> float:
        """Test on dataset and calculate accuracy."""
        val = np.array(validation_set)
        # pylint: disable=unbalanced-tuple-unpacking
        x_val, y_val = (
            val[:, :val.shape[1] - 1],
            val[:, val.shape[1] - 1:]
        )

        message = (
            "\n"
            "Test dataset statistics:\n"
            "------------------------------\n"
            f"Records: {x_val.shape[0]}\n"
            f"Dimensions: {x_val.shape[1]}\n"
            f"Classes: {np.unique(y_val).shape[0]}"
        )
        self._logger.debug(message)

        score = 0

        for i, point in enumerate(x_val):
            self._logger.info(
                'Validating point %d out of %d',
                i + 1, x_val.shape[0]
            )
            score += int(
                self.predict(point=point, K=K) == y_val[i]
            )

        score /= val.shape[0]

        self._logger.warning('Test accuracy: %s', score)
        return score


def cross_val(
        dataset: np.ndarray,
        max_K: int = 20,
        return_stats: bool = False
) -> Union[int, tuple]:
    max_K += 1
    log = get_colored_logger('Cross validation')
    stats = {}
    # print(dataset)
    for K in range(1, max_K):
        log.info('Validating K = %d', K)
        score = 0
        for i in range(len(dataset)):
            point = dataset[i]
            test = np.delete(dataset, i, 0)
            test_label = point[-1:]
            test_point = point[:-1]

            # print("test", test_point)

            knn = KNN()
            knn.train(test)
            predict = knn.predict(test_point, K=K)

            score += predict == test_label

        stats[K] = float(score / len(dataset))
        log.warning('Score for K = %d is %f', K, stats[K])

    best_K = max(stats.items(), key=operator.itemgetter(1))[0]
    log.warning('Best K is %s', best_K)

    if return_stats:
        return best_K, stats
    return best_K


def run():
    """Run KNN as test."""
    dataset = np.genfromtxt(
        Path('./datasets/banknote_authentication_no_label.csv'),
        delimiter=','
    )
    best_K, stats = cross_val(dataset, max_K=2, return_stats=True)
    draw_bar_chart(stats, best_K)


if __name__ == '__main__':
    run()
